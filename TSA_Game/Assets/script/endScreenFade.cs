﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class endScreenFade : MonoBehaviour {
    public Image fadeImage;
    float fadeScreenAlpha = 1;
    Color fade = Color.white;

    void Start () {
        fadeImage.color = Color.white;
        StartCoroutine(fadeOut());
    }
    IEnumerator fadeOut()
    {
        yield return new WaitForSeconds(0.01f);
        if (fadeScreenAlpha > 0)
        {
            fadeScreenAlpha -= 0.02f;
            fade.a = fadeScreenAlpha;
            fadeImage.color = fade;
            StartCoroutine(fadeOut());
        }

    }
}
