﻿using UnityEngine;
using System.Collections;

public class upgrade : MonoBehaviour {

	public GameObject mediumcoal;
	public GameObject bigcoal;
	GameObject earth;

	int mediumPlantCost  = 4000;
	int bigPlantCost = 20000;
	// Use this for initialization
	void Start () {
		earth = Backend.earth;
	}

	public void upgradesmallcoal()
	{
		if (mediumPlantCost <= Backend.money) {
            
            Backend.coalobjects[plantNumber()] = (GameObject)Instantiate (mediumcoal, new Vector3 (earth.transform.position.x, earth.transform.position.y, -5), Quaternion.Euler (new Vector3 (0, 0, this.transform.rotation.eulerAngles.z)));
			changeLevelArray();
			Backend.money -= mediumPlantCost;
			GameObject.Destroy (this.gameObject);
            
        }
        

    }
	public void upgrademediumcoal()
	{
		if (bigPlantCost <= Backend.money) {
			Backend.coalobjects[plantNumber()] = (GameObject)Instantiate (bigcoal, new Vector3 (earth.transform.position.x, earth.transform.position.y, -5), Quaternion.Euler (new Vector3 (0, 0, this.transform.rotation.eulerAngles.z)));
			changeLevelArray ();
			Backend.money -= bigPlantCost;
			GameObject.Destroy (this.gameObject);
		}
	}

	public void changeLevelArray()
	{
		int lvl = plantNumber ();

		Backend.pplvl [lvl]++;

	}

	public int plantNumber()
	{
		if (this.transform.rotation.eulerAngles.z != 0) {
			int lvl = (360 - (int)this.transform.rotation.eulerAngles.z) / 45;
			return lvl;
		
		} else {
			return 0;
		}

	}

	IEnumerator Wait()
	{
		GameObject upgradeMenu = GameObject.Find ("upgradeMenu");

		yield return new WaitForSeconds(0.5f);
	
		upgradeMenu.GetComponent<Animator> ().SetBool ("upgradeMenuToggle", true);
	}


	public void setUpgradeNum(GameObject game)
	{
		GameObject upgradeMenu = GameObject.Find ("upgradeMenu");

		if (plantNumber () != Backend.UpgradeNumber) {

			upgradeMenu.GetComponent<Animator> ().SetBool ("upgradeMenuToggle", false);
			StartCoroutine (Wait ());
		


		}


		Backend.UpgradeNumber = plantNumber ();
	
		/*
		string name = game.name.ToString ();
		char[] letters = name.ToCharArray ();
		int upgradeNum = int.Parse(letters [letters.Length - 1].ToString());

	

		*/
	}


}
