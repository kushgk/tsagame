﻿using UnityEngine;
using System.Collections;

public class StaticReset : MonoBehaviour {

	public static float RenewablesStartCost = 100000;
	public static float StartMoney = 2000;
	public static float StartPeople = 5;
	public static int StartHousing = 25;

	// Use this for initialization
	void Start () {


		//planet based
		Backend.coalobjects = new GameObject[8];
		Backend.pplvl = new int[8];
		Backend.RNobjects = new GameObject[8];
		Backend.tornadoActive = new bool[8];

		//perks
		Backend.Emissionsboost = 0;
		Backend.energyboost = 0;
		Backend.toughnessboost = 0;
		Backend.RNenergyboost = 0;

		//Starting Stats
		Backend.money = StartMoney;
		Backend.people = StartPeople;
		Backend.renewablecost = RenewablesStartCost;
		Backend.UpgradeNumber = 0;
		Backend.renewablescount = 0;
		Backend.housing = 0;


		AudioListener.pause = false;



	



	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
