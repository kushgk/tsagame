﻿using UnityEngine;
using System.Collections;

public class TutorialNumber7ArrowPosition : MonoBehaviour {

	public GameObject capAndTradeBar;
	Vector3 pos;
	// Update is called once per frame
	void Update () {
		pos = transform.position;
		transform.position = new Vector3 (capAndTradeBar.transform.position.x,pos.y,pos.z);
	}
}
