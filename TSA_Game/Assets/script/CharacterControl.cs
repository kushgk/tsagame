﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterControl : MonoBehaviour
{
    private Animator flipAnimator;
    private Animator spinAnimator;
    GameObject speakBox;
    Text message;
    public static int messageNumber;
    string[] messageStrings = new string[] {
        "I wish my electricity bills were cheaper...",
"I accidentally threw all my paper in the trash...",
"Ugh...I hate that coal plant near my house...",
"You hear about the death of the gorilla?",
"Hmm...maybe I should get an electric car...",
"Wow what a hot day!",
"I wish I got the house with a pool.",
"there are too many people in this city...",
"Hey look, I can light my water on fire!",
"My crops are dying!",
"Did you hear about that breakthrough in sustainable tech?",
"Who should I vote for this election...hm...",
"I can't stand the heat!",
"No way Toland Drump is getting my vote",
"Wow that renewable plant sure looks fancy.",
"I'm definitely voting for Toland Drump!"
    };
    public static int charNumber;
    int myCharNumber;
    
    // Use this for initialization
    void Start()
    {
        

        if(charNumber==null)
        {
            charNumber = 0;
        }
        charNumber++;
        myCharNumber = charNumber;
        speakBox = GameObject.Find("speakBox");
		if(SceneManager.GetActiveScene().name.Equals("MainGame"))
		{
        	message = GameObject.Find("speakBox/Canvas/Text").GetComponent<Text>();
		}
        flipAnimator = gameObject.transform.GetChild(0).GetComponent<Animator>();
        spinAnimator = GetComponent<Animator>();
        flipAnimator.SetBool("charFlipBool", true);
        spinAnimator.SetBool("charSpinBool", true);
    }
    void Update()
    {
        if(SpeakCharNumber.timer)
        {
            ActivateMessageBox();
        }
    }
    void ActivateMessageBox()
    {
        if (SpeakCharNumber.GetSpeakNumber() == myCharNumber)
        {
            StartCoroutine(WaitKillBox(8F));
            speakBox.transform.rotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
            if ((transform.eulerAngles.z > (360 + 90) && transform.eulerAngles.z < (360 + 180)) || (transform.eulerAngles.z < (360 - 90) && transform.eulerAngles.z > (360 - 180)) || (transform.eulerAngles.z < (-90) && transform.eulerAngles.z > (-180)) || (transform.eulerAngles.z > (90) && transform.eulerAngles.z < (180)))
            {
                message.transform.localEulerAngles = new Vector3(0, 0, 180);
            }
            else
            {
                message.transform.localEulerAngles = new Vector3(0, 0, 0);
            }
            speakBox.gameObject.transform.GetChild(0).GetComponent<Animator>().SetBool("activateSpeakBox", true);
            spinAnimator.speed = 0;
            flipAnimator.speed = 0;
            flipAnimator.SetBool("charFlipBool", false);
            spinAnimator.SetBool("charSpinBool", false);
            message.text = messageStrings[messageNumber];
            messageNumber++;
            SpeakCharNumber.timer = false;
        }
    }

    IEnumerator WaitKillBox(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        speakBox.gameObject.transform.GetChild(0).GetComponent<Animator>().SetBool("activateSpeakBox", false);
        spinAnimator.speed = 1;
        flipAnimator.speed = 1;
        flipAnimator.SetBool("charFlipBool", true);
        spinAnimator.SetBool("charSpinBool", true);

    }
  
    
}
