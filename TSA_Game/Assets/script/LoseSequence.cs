﻿using UnityEngine;
using System.Collections;

public class LoseSequence : MonoBehaviour {
	public GameObject explosionSpin;
	float randomZAngle;
	// Use this for initialization
	void Start () {
		StartCoroutine (WaitAndSpawn ());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	IEnumerator WaitAndSpawn()
	{
		yield return new WaitForSeconds (0.5f);
		randomZAngle = Random.Range (0, 360);
		GameObject go = Instantiate (explosionSpin, transform.position, Quaternion.Euler(0,0,randomZAngle)) as GameObject;
		go.transform.SetParent (transform);
		go.gameObject.GetComponentsInChildren<Animator> ()[0].SetBool("explode",true);
		StartCoroutine (WaitAndSpawn ());
	}
}
