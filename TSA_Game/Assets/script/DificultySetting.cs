﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI; 

public class DificultySetting : MonoBehaviour {
    public GameObject scroll;

	public Image mediumButton;
	public Image hardButton;

	GameObject mediumLock;
	GameObject hardLock;

    bool next = false;
    bool previous = false;
    bool nextInit = true;
    bool previousInit = true;

    float scrollSpeed;
    float InitScrollSpeed=0.5f;
    float scrollAcceleration = 0.05f;

    int scrollCount = 0;

    Vector3 scrollPosTemp;
    float scrollPosInit;
	Color LockedColor;

	int ppDifficulty;
	 
    // Use this for initialization
    void Start () {
        scrollSpeed = InitScrollSpeed;
        scrollPosInit = scroll.transform.position.x;
		mediumButton = GameObject.Find ("Medium").GetComponent<Image>();
		hardButton = GameObject.Find ("Hard").GetComponent<Image>();

		mediumLock = GameObject.Find ("mediumLock");
		hardLock = GameObject.Find ("hardLock");

		mediumLock.SetActive (false);
		hardLock.SetActive (false);


		LockedColor = mediumButton.color;
		LockedColor = new Color (LockedColor.r - 0.3f, LockedColor.g - 0.3f, LockedColor.b - 0.3f, LockedColor.a);

		ppDifficulty = PlayerPrefs.GetInt ("Difficulty");


    }
	
	// Update is called once per frame
	void FixedUpdate () {

		if (ppDifficulty < 1 && startScreen.switchMenu) {
			mediumButton.color = LockedColor;
			mediumLock.SetActive (true);
		}
		if (ppDifficulty < 2 && startScreen.switchMenu) {
			hardButton.color = LockedColor;
			hardLock.SetActive (true);
		}
	

        if (next) {
            if (nextInit) {
                scrollPosTemp = scroll.transform.position;
                nextInit = false;
            }
            scrollSpeed += scrollAcceleration;
            if (scroll.transform.position.x > scrollPosTemp.x - (17.774f - scrollAcceleration*30))
            {
                scroll.transform.position = new Vector3(scroll.transform.position.x - 1 * scrollSpeed, scroll.transform.position.y, scroll.transform.position.z);
            }
            else
            {
                scrollCount++;
                scrollSpeed = InitScrollSpeed;
                next = false;
                nextInit = true;
                scroll.transform.position = new Vector3(scrollPosTemp.x - 17.774f, scroll.transform.position.y, scroll.transform.position.z);
                foreach (Transform child in scroll.transform)
                {
                    Vector3 ct = child.transform.position;
                   
                    if (ct.x < scrollPosInit - 17.774f * 2f+0.1f)
                    {
                        child.transform.position = new Vector3(ct.x + 17.774f * 3, ct.y, ct.z);
                    }
                }
            }
        }
        if (previous)
        {
            if (previousInit)
            {
                scrollPosTemp = scroll.transform.position;
                previousInit = false;
            }
            scrollSpeed += scrollAcceleration;
            if (scroll.transform.position.x < scrollPosTemp.x + (17.774f - scrollAcceleration*30))
            {
                scroll.transform.position = new Vector3(scroll.transform.position.x + 1 * scrollSpeed, scroll.transform.position.y, scroll.transform.position.z);
            }
            else
            {
                scrollCount++;
                scrollSpeed = InitScrollSpeed;
                previous = false;
                previousInit = true;
                scroll.transform.position = new Vector3(scrollPosTemp.x + 17.774f, scroll.transform.position.y, scroll.transform.position.z);
                foreach (Transform child in scroll.transform)
                {
                    Vector3 ct = child.transform.position;
                    if (ct.x > scrollPosInit + 17.774f * 2f - 0.1f)
                    {
                        child.transform.position = new Vector3(ct.x - 17.774f * 3, ct.y, ct.z);
                    }
                }
            }
        }
        
    }

	public void SetEasyDifficulty()
	{
		Backend.difficulty = 50;
		SceneManager.LoadScene("Loading" );
	
	}
	public void SetMediumDifficulty()
	{
		if (PlayerPrefs.GetInt ("Difficulty") > 0) {
			Backend.difficulty = 25;
			SceneManager.LoadScene ("Loading");
		}
		else {
			Debug.Log ("You need to beat easy mode first");
		}

	}
	public void SetHardDifficulty()
	{
		if (PlayerPrefs.GetInt ("Difficulty") > 1) {
			Backend.difficulty = 0;
			SceneManager.LoadScene ("Loading");
		}
		else {
			Debug.Log ("You need to beat medium mode first");
		}
	}
    public void nextButton()
    {
        next = true;
    }
    public void previousButton()
    {
        previous = true;

    }
}
