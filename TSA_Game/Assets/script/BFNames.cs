﻿using UnityEngine;
using System.Collections;

public class BFNames : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public static string setString(string name , int num)
	{
		if (name.Equals ("battery")) {
			perks.des.text = "Allow cities to institute centralized power production, increasing the number of people cities can support.";
			switch (num) {
			case 1:
				return "Centralized Power Production 1";
			case 2:
				return "Centralized Power Production 2";
			case 3:
				return "Centralized Power Production 3";
			case 4:
				return "Centralized Power Production 4";
			}

		}
		if (name.Equals ("lobby")) {
			perks.des.text = "Hire Lobbyists to prevent expensive anti-coal legislation from being passed.";
			switch (num) {
			case 1:
				return "Lobbying 1";
			case 2:	   
				return "Lobbying 2";
			case 3:	   
				return "Lobbying 3";
			case 4:	
				return "Lobbying 4";
			}

		}
		if (name.Equals ("maintainance")) {
			perks.des.text = "Build storm defenses to increase toughness of cities and reduce inevitable damage from climate change.";
			switch (num) {
			case 1:
				return "Adaptation 1";
			case 2:	   
				return "Adaptation 2";
			case 3:	   
				return "Adaptation 3";
			case 4:	
				return "Adaptation 4";
			}

		}
		if (name.Equals ("efficiency")) {
			perks.des.text = "Increase conductivity of wires to increase efficiency of energy transfer and allow for cheaper electricity.";
			switch (num) {
			case 1:
				return "Superconductive Wires 1";
			case 2:	 
				return "Superconductive Wires 2";
			case 3:	
				return "Superconductive Wires 3";
			case 4:	
				return "Superconductive Wires 4";
			}

		}
		if (name.Equals ("CCS")) {
			perks.des.text = "Store carbon emissions underground to prevent them from causing warming.";
			switch (num) {
			case 1:
				return "Carbon Sequestration 1";
			case 2:	 
				return "Carbon Sequestration 2";
			case 3:	
				return "Carbon Sequestration 3";
			case 4:	
				return "Carbon Sequestration 4";
			}

		}
		if (name.Equals ("RNbattery")) {//2nd column
			



			switch (num) {
			case 1:
				perks.des.text = "First generation biofuels are the 'original' biofuels. They are less expensive than second generation, but if used on large scales are unsustainable";
				return "First Generation Biofuels";

			case 2:
				perks.des.text = "Ethanol fuel is a mix of 85% ethanol and 15% Petroleum. This fuel source is made from the sugars of corn and sugarcane.";
				return "Ethanol";

			case 3:	
				perks.des.text = "Biodiesel refined from animal fats and vegtable oils. This fuel can be used to power cars and is much cleaner than it's diesel counterpart.";
				return "Biodiesel";

			case 4:
				perks.des.text = "Made from hydrocracking oil and fat feedstock. Most efficient variety of first generation fuels";
				return "Green Diesel";	



			}

		}

		if (name.Equals ("trees")) {
			switch (num) {
			case 1:
				perks.des.text = "A greener, and more sustainable variety of biofuels, more costly, but more benefits";
				return "Second Generation Biofuels";
			case 2:
				perks.des.text = "Cellulosic power is usually made from wood, grass, or inedible parts of plants. Doesn't cause a loss in food production, but is costly";
				return "Cellulosic Power";

			case 3:	  
				perks.des.text = "Algae based fuels are energy rich. The CO2 emitted is offset by the photosynthesis production of algae plants.";
				return "Algae";

			case 4:	   
				perks.des.text = "Hydrogen produced biologically by organisms such as algae and bacteria. Burning it does not produce any CO2.";
				return "Biohydrogen";
			}

		}
		if (name.Equals ("RNreasearch")) {
			
			switch (num) {
			case 1:
				perks.des.text = "Construct a research facility to perform more research into biofuels technologies.";
				return "Research Facility";
			case 2:	
				perks.des.text = "Research genetically modified corn. Increases corn yeilds allowing for more ethanol production";
				return "Genetically Modified Corn";
			case 3:
				perks.des.text = "Research methods for controling overgrowth of algae. This maximizes the algae's photosynthetic ablities.";
				return "Algae Population Control";
			case 4:	 
				perks.des.text = "Research algae conversion technique. Algae are used to convert CO2 from power plants to energy";
				return "Algae Cogeneration";
			}

		}

		if (name.Equals ("EOS")) {
			
			switch (num) {
			case 1:
				perks.des.text = "Increase biofuels production infrastructure drastically reducing the cost of production.";
				return "Economies of Scale";

			case 2:
				perks.des.text = "Modernize biofuels production infrastructure drastically reducing the cost of production.";
				return "Infrastructure Development";

			
			}

		}
		if (name.Equals ("NG")) {
			perks.des.text = "Utilize cleaner burning alternatives to coal, such as natural gas, for energy production.";
			switch (num) {
			case 1:
				return "Alternative Fossil Fuels 1";
			case 2:	
				return "Alternative Fossil Fuels 2";
			case 3:
				return "Alternative Fossil Fuels 3";
			case 4:	 
				return "Alternative Fossil Fuels 4";
			}

		}

		return "";


	}
}
