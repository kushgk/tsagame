﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class startScreen : MonoBehaviour {

    public GameObject startMenu;
    public GameObject difficultyMenu;

	public static GameObject startMenu2;

	public static bool switchMenu;

	// Use this for initialization
	void Start () {
		startMenu2 = startMenu;
		switchMenu = false;

		//PLAYER PREFS ACTION!!!

		if(!PlayerPrefs.HasKey("FirstPlay"))
		{
			PlayerPrefs.SetInt ("FirstPlay", 0);
			PlayerPrefs.SetInt ("Difficulty", 0);
			PlayerPrefs.SetInt("firstTime", 0);
		}
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Space)) {
			PlayerPrefs.SetInt ("Difficulty", 0);
		}
	
	}
	public void startGame(){
        //SceneManager.LoadScene("DifficultyMenu");
		startMenu.SetActive(false);
		difficultyMenu.SetActive(true);
		switchMenu = true;

	}
	public void quitGame(){
		Application.Quit ();
	}
	public void creditGame()
	{
		SceneManager.LoadScene ("Credits");
	}



}
