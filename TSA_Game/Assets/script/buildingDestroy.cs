﻿using UnityEngine;
using System.Collections;

public class buildingDestroy : MonoBehaviour {
    public static int buildingNum;
    private int myBuildingNum;
    private bool destructionCheck=true;
	// Use this for initialization
	void Start () {
        if (buildingNum == null)
        {
            buildingNum = 0;
        }
        else { buildingNum++; }
        myBuildingNum = buildingNum;
	}
	
	// Update is called once per frame
	void Update () {
        if (Backend.housing < Backend.people && destructionCheck)
        {

            destroyBuilding();
        }
    }

    private void destroyBuilding()
    {
        //Debug.Log(myBuildingNum);
        if (myBuildingNum == building.destructionNum)
        {
            foreach (Transform child in transform)
            {
                if (child.name == "explosion")
                {
                    child.GetComponent<Animator>().SetBool("explode", true);
                }
            }

            foreach (Transform child in transform)
            {
                if (child.name == "wiggle")
                {
                    child.gameObject.SetActive(false);
                }
            }
            destructionCheck = false;
        }
        
    }
}
