﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Settings : MonoBehaviour {



	public GameObject volumeOn1;
	public GameObject volumeOn2;
	public GameObject volumeOff1;
	public GameObject volumeOff2;
    
	Text tutText;

	bool volume = false;
	// Use this for initialization
	void Start () {
		tutText = GameObject.Find ("tutText").GetComponent<Text>();

		if (PlayerPrefs.GetInt ("firstTime") == 1) {
			
			tutText.text = "Tutorial: Off";
		}
		else {
			
			tutText.text = "Tutorial: On";
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void resetPlayerPrefs(Text t)
	{
		if (PlayerPrefs.GetInt ("firstTime") == 1) {
			PlayerPrefs.SetInt ("firstTime", 0);
			t.text = "Tutorial: On";
		}
		else {
			PlayerPrefs.SetInt ("firstTime", 1);
			t.text = "Tutorial: Off";
		}

        
    }


	public void ToggleVolume ()
	{

	

		volume = !volume;
		AudioListener.pause = volume;
		if (!volume) {
			volumeOn1.SetActive (true);
			volumeOn2.SetActive (true);
			volumeOff1.SetActive (false);
			volumeOff2.SetActive (false);
			
		} else {
			volumeOn1.SetActive (false);
			volumeOn2.SetActive (false);
			volumeOff1.SetActive (true);
			volumeOff2.SetActive (true);

		}

	}
}
