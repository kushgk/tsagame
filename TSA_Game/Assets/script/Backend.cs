﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; 
using UnityEngine.SceneManagement;

public class Backend : MonoBehaviour {

	//static variables declared in top
	public GameObject timeTxt;
	public GameObject coalTxt;
	public GameObject peopleTxt;
	public GameObject housingTxt;
	public GameObject MoneyTxt;

	public Text EmissionsText;
	public Text EmissionsRateText;
	public Text upgradeCostText;
	public Text capAndTradeText;
	public Text StockMarket;
	static Text moneytext;
	static Text timetext; 
	static Text coaltext;
	static Text peopletext;
	static Text housingtext;
	static Text Moneytext;
	public Text RNtext;
	public Text moneyRate;
	public Text popRate;
	public Text timeUntilTradetext;

	public Image coaldraggable;
	public Image RNdraggable;
	public Image circularGWbar;
	public Image emissionBar;

	public Image nrnShade;
	public Image rnShade;
	public Image timeTradeCircle;
	public Image warningPanel;
    public Text warningText1;
    public Text warningText2;

	public GameObject capAndTrade;
	float capAndTradeNum;
	public ParticleSystem capAndTradeParticleEffect;

    public GameObject bigcoal;
	public GameObject mediumcoal;
	public GameObject smallcoal;
	public GameObject renewable;
	public GameObject plantAvailable;


	public Sprite easyEarth;
	public Sprite mediumEarth;
	public Sprite hardEarth;
	public static GameObject earth;

	public GameObject upbutton;
	public GameObject tornado;
	public GameObject GWbar;
	public GameObject GWcircle;
	public GameObject smokepoof;
	public static GameObject upgradeMenu;


	//Losing Sequence and Winning Sequence
	public GameObject loseSequenceGO;
	public GameObject winSequenceGO;



	//Game settings
	public static int difficulty;//higher is easier
	const int GAMETIME = 1;//doesn't do much	
	const int MAXPLANTS = 8;//max plants
	const float PLANETSIZE = 3f;//for planet change
	const float POPGROWTH = .008f * 0.2f;
	float MAXEM = 10000000 * (float)(1 + (float)difficulty/75.0); // increase MaxEM based on difficulty
	public bool developerMode = false;

	//money stuff
	public static float money;
	bool bankrupt = false;
	static string moneyString;

	//people stuff
	public static float people;
	static string popString;

	//housing
	static int minhousing;
	static string houseString;
	public static int housing = 0;

	//Coal stats
	public static int coalPower = 50;
	int coalPlantCost = 500 * GAMETIME;
	int teir2cost = 4000 * GAMETIME;
	int teir3cost = 20000 * GAMETIME;
	float nextupcost = 2;
	public static int UpgradeNumber = 0;

	//coal arrays
	int coalplants = 0;
	public static int[] pplvl = new int[MAXPLANTS];
	public static GameObject[] coalobjects = new GameObject[MAXPLANTS];
	public static GameObject[] RNobjects = new GameObject[MAXPLANTS];
	bool[] deadplant = new bool[MAXPLANTS];
	int[] clicks = new int[MAXPLANTS];

	//renewables
	public static float renewablecost;
	public static int renewablescount = 0;
	public static float RNpower = 25;
	public static float RNenergyboost = 0;

	//tornados
	public static bool[] tornadoActive = new bool[MAXPLANTS];
	float stormchance;
	float warningLerpSpeed = 0.05f;

	//Time stuff
	int wholeTimer = 0;
	float time = 0;
	bool gameover = false;
	bool bad = false;
	float badtime = 0;
	bool delaytimerdone = true;
	float delaytimer = 0;
	float inittime = 0;
	float initvalue = 0;
	int Interval = 30;
	bool youLose = false;

	//change in stats
	float deltaPopulation;
	float deltaMoney;

	//PERKS!!!
	public static float Emissionsboost = 0;
	public static float toughnessboost = 0;
	public static float energyboost = 0;
	public static float moneyboost = 0;

	//Emissions Stuff
	float Emissionsgain = 0;
	float EmFrac = 0;
	float Emissions = 1000000;

	//cap and trade + Warning
	float initbarpos;
	float capTextValue;
	float peopleTextAlarm =0;
	bool peopleTextAlarmSwitch = true;
	float warningNumber=0;
	float capBonus;

	//power plant positioning
	double xshift=0;
	double yshift=0;
	float pxpos;
	float pypos;
	float prot;

	//dragon drop
	public bool drag = false;
	public static bool coaldrag = false;
	public bool RNdrag = false;
	GameObject tempdrag;



	//no idea what it does right now
	public bool cacheReset;

    public Image fadeScreen;


	bool lockPopulationForFirstFewSeconds=true; //population number jumps around for the first fer seconds so

	void Start (){//----------------------------------------------------------------------------------------------------------------------------------------------------------------
        
		if (cacheReset) {
			//PlayerPrefs.DeleteAll ();
			cacheReset = false;
		}

		earth = GameObject.Find("earth6");

		//text changes
	    timetext    = timeTxt.GetComponent<Text>();
 	    coaltext    = coalTxt.GetComponent<Text>();
		peopletext	= peopleTxt.GetComponent<Text> ();
		housingtext	= housingTxt.GetComponent<Text>();
		Moneytext	= MoneyTxt.GetComponent<Text>();
		Moneytext.text = money.ToString();

		//initialization
		emissionBar.fillAmount = 0;
		capAndTradeNum = 0.3f;
		//capAndTrade.transform.localPosition = new Vector3(0.3f*308f,capAndTrade.transform.localPosition.x,capAndTrade.transform.localPosition.y);
		capAndTradeText.text = "";
		upgradeMenu = GameObject.Find ("upgradeMenu");
		xshift = earth.transform.position.x;
		yshift = earth.transform.position.y;
		money = StaticReset.StartMoney;
		people = StaticReset.StartPeople;
		renewablecost = StaticReset.RenewablesStartCost;
		minhousing = StaticReset.StartHousing;

		//change difficulty
		coalPower = difficulty;
		RNpower = difficulty;

        warningPanel.color = new Vector4(0, 0, 0, 0);
        warningText1.color = new Vector4(0, 0, 0, 0);
        warningText2.color = new Vector4(0, 0, 0, 0);

        //
        if (difficulty == 50)
		{
			earth.gameObject.GetComponent<SpriteRenderer> ().sprite = easyEarth;
		}
		else if (difficulty == 25)
		{
			earth.gameObject.GetComponent<SpriteRenderer> ().sprite =mediumEarth;
		}
		else if (difficulty == 0)
		{
			earth.gameObject.GetComponent<SpriteRenderer> ().sprite = hardEarth;
		}

		StartCoroutine (setPeople ());

	} 
	IEnumerator setPeople()
	{
		yield return new WaitForSeconds (0);
		lockPopulationForFirstFewSeconds = false;
	}

	void FixedUpdate () {
		
		//if the game is over ----------------------------------------------------------------------------------------------------------------------------------------------------------------
		if (Emissionsgain < 0 && housing > people){
            //SceneManager.LoadScene("win");
			if (difficulty == 50) {
				PlayerPrefs.SetInt ("Difficulty", Mathf.Max (1, PlayerPrefs.GetInt ("Difficulty")));

			}
			if (difficulty == 25) {
				PlayerPrefs.SetInt ("Difficulty", Mathf.Max (2, PlayerPrefs.GetInt ("Difficulty")));
			}
			if (difficulty == 0) {
				PlayerPrefs.SetInt ("Difficulty", Mathf.Max (3, PlayerPrefs.GetInt ("Difficulty")));
			}


            //StartCoroutine(fadeOut("win"));
			StartCoroutine(winSequence());
			winSequenceGO.SetActive (true);
        } 
		else if (youLose || Emissions >= MAXEM)
		{
			//SceneManager.LoadScene("lose");
            //StartCoroutine(fadeOut("lose"));
			StartCoroutine(loseSequence());
			loseSequenceGO.SetActive (true);
			people = 0;
        }




		//String Stuff------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		houseString = null;
		moneyString = null;
		popString = null;

		//housing string manipulation
		for (int i = 0; i < 7 - housing.ToString ().Length; i++) {
			houseString += "0 ";
		}
		int statStringLoopNum = 0;
		foreach (char car in housing.ToString()) {
			houseString += car;
			if(statStringLoopNum!=housing.ToString().Length-1){
				houseString+=" ";
			}
			statStringLoopNum++;
		}
		statStringLoopNum = 0;

		//money string manipulation
		for (int i = 0; i < 7 - ((int)money).ToString ().Length; i++) {
			moneyString += "0 ";
		}
		foreach (char car in ((int)money).ToString ()) {
			moneyString += car;
			if(statStringLoopNum!=((int)money).ToString ().Length-1){
				moneyString+=" ";
			}
			statStringLoopNum++;
		}
		statStringLoopNum = 0;

		//population string manipulation
		for (int i = 0; i < 7 - ((int)people).ToString ().Length; i++) {
			popString += "0 ";
		}
		foreach (char car in ((int)people).ToString ()) {
			popString += car;
			if(statStringLoopNum!=((int)people).ToString ().Length-1){
				popString+=" ";
			}
			statStringLoopNum++;
		}

		//update texts
		timetext.text = houseString; //actually housing
		Moneytext.text = moneyString;
		peopletext.text = popString;	
		timetext.text = houseString; //actually housing
		Moneytext.text = moneyString;
		peopletext.text = popString;
		RNtext.text = "$" + ((int)renewablecost).ToString ();
		coaltext.text = "$" + ((int)coalPlantCost).ToString ();


		//upgrade cost
		if(pplvl[UpgradeNumber] == 1)
		{
			upgradeCostText.text = "$"+teir2cost.ToString();
		}
		if(pplvl[UpgradeNumber] == 2)
		{
			upgradeCostText.text = "$"+teir3cost.ToString();
		}


        //Cheat Keys------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		if (developerMode)
		{
			if (Input.GetKey (KeyCode.A)) {
				money += 1000000;
			}
			if (Input.GetKey (KeyCode.S)) {
				minhousing += 50;
			}
			if (Input.GetKey (KeyCode.D)) {
				people = 1050;
			}
			if (Input.GetKey (KeyCode.F)) {
				Emissions += 10000;
			}
			if (Input.GetKey (KeyCode.G)) {
				money += 1000;
			}
			if (Input.GetKey (KeyCode.H)) {
				people += 10;
			}
			if (Input.GetKey (KeyCode.J)) {
				Emissions += 1000000;
			}
			if (Input.GetKey (KeyCode.K)) {
				Time.timeScale = 10;
			} else {
				Time.timeScale = 1;
			}
		}
       
        

		//Coloring Stuff-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//population red alarm
		if(people>=housing){
			if(peopleTextAlarmSwitch && peopleTextAlarm<1)
			{
				peopleTextAlarm+=0.05f;	
			}
			else
			{
				peopleTextAlarmSwitch=false;
			}
			if(!peopleTextAlarmSwitch && peopleTextAlarm>0)
			{
				peopleTextAlarm-=0.05f;
			}
			else
			{
				peopleTextAlarmSwitch=true;
			}
			
			//lerp black to red
			peopletext.color =Color.Lerp(new Color(66f/255f,66f/255f,66f/255f), new Vector4(255f/255f, 86f/255f, 86f/255f, 1), peopleTextAlarm);

			//warning panel lerp
			warningNumber += warningLerpSpeed;
			if (warningNumber > 1 || warningNumber < 0)
			{
				warningLerpSpeed *= -1;
			}
			warningPanel.color =Color.Lerp(new Vector4(66f/255f,66f/255f,66f/255f,0), new Vector4(255f/255f, 86f/255f, 86f/255f, 0.4f), warningNumber);
            warningText1.color = Color.Lerp(new Vector4(255f, 255f, 255f, 0.5f), new Vector4(255, 255f,255f, 0.9f), warningNumber);
            warningText2.color = Color.Lerp(new Vector4(255f, 255f, 255f, 0.5f), new Vector4(255, 255f, 255f, 0.9f), warningNumber);
        }
		else
		{
			warningPanel.color = new Vector4 (0, 0, 0, 0);
            warningText1.color = new Vector4(0, 0, 0, 0);
            warningText2.color = new Vector4(0, 0, 0, 0);
            peopletext.color =new Color(66f/255f,66f/255f,66f/255f);
		}
			
		//power plant coloring
		if (money > coalPlantCost) 
		{

			nrnShade.enabled = false;
		} 
		else
		{
			nrnShade.enabled = true;
		}
		if (money >renewablecost)
		{

			rnShade.enabled = false;
		}
		else
		{
			rnShade.enabled = true;
		}

		//cap and trade Coloring
		if (wholeTimer <= 600) {
			//capAndTrade.fillAmount = 0.2f + (float)wholeTimer * 1 / 600; 
			Vector3 pos=capAndTrade.transform.position;
			capAndTradeNum = (0.2f + (float)wholeTimer * 1 / 600) ; //I took away the * 308 and instead multiplied by 600 when needed

			capAndTrade.transform.localPosition = new Vector3(capAndTradeNum * 600f, pos.y+4.5f, pos.z);
			pos=capAndTrade.transform.position;
		
			ParticleSystem.ShapeModule shapep = capAndTradeParticleEffect.GetComponent<ParticleSystem> ().shape;
	
			//shapep.box = new Vector3 (Mathf.Abs((capAndTradeNum - emissionBar.fillAmount)*12)- (4.0f/600.0f) * 12, 1, 1); // added the 4/600 * 13 to stop particle affect from going over the bar
			if ((capAndTradeNum - emissionBar.fillAmount) > 0) {
				shapep.box = new Vector3 (Mathf.Abs((capAndTradeNum - emissionBar.fillAmount)*12)- (4.0f/600.0f) * 12, 1, 1);
				capAndTradeParticleEffect.transform.localPosition = new Vector3 (capAndTradeNum * 616.0f - (shapep.box.x / 2 / 13) * 600.0f - 7f, pos.y - 7, pos.z);
				capAndTradeParticleEffect.startColor = new Color (255f/255f,219f/255f,101f/255f,69f/255f);
			} else {
				shapep.box = new Vector3 (Mathf.Abs((capAndTradeNum - emissionBar.fillAmount)*13), 1, 1);
				capAndTradeParticleEffect.transform.localPosition = new Vector3 (capAndTradeNum * 616.0f + (shapep.box.x / 2 / 13) * 600.0f, pos.y - 7, pos.z);
				capAndTradeParticleEffect.startColor = new Color (0,0,0,160f/255f);
			}
			ParticleSystem.EmissionModule em = capAndTradeParticleEffect.GetComponent<ParticleSystem> ().emission;
			em.rate = Mathf.Abs((capAndTradeNum - emissionBar.fillAmount) * 5000);
		}


		//time stuff----------------------------------------------------------------------------------------------------------------------------------------------------------------------
		time += Time.deltaTime;
		timeTradeCircle.fillAmount = ((float)(time % Interval)) / Interval;

		//operations that take place every whole second
		if (wholeTimer != (int)time) {
			
			wholeTimer = (int)time;


			//for fun stats
			StockMarket.text = ((int)(((housing - people) / people) * 100)).ToString() + " pts";

			//cap and trade
			timeUntilTradetext.text = (Interval - (wholeTimer % Interval)) + "";
			capBonus = 1000 * (capAndTradeNum - emissionBar.fillAmount) * (housing * 2.0f / 100.0f);
			capAndTradeText.text = "$"+((int)( capBonus )).ToString();


			if((wholeTimer) % 20 == 0)// every 20 seconds
			{
					NewsEvents.changeNewsText();
			}

			if (wholeTimer % Interval == 0) //every 30 seconds
			{
				NewsEvents.changeNewsText();
				money += capBonus; 
			}
		}

	
        //pausing
        if (!ui.gamePause)
        {
            //housing stuff-----------------------------------------------------------------------------------------------------------------------------------------------------------------

            //main housing evaluation
            housing = minhousing;//minimum
            for (int i = 0; i < pplvl.Length; i++)
            {
                if (pplvl[i] != 0)
                {
					housing += (int)(Mathf.Pow(2, (float)pplvl[i] - 1) * (coalPower + 100 / 2.0) ); // housing is 2^lvl-1 * coal power
                }

            }
            housing += (int)(renewablescount * RNpower * (1 + RNenergyboost / 100));//renerables power
            housing = (int)(housing * (1 + energyboost / 100));


            //people growth-------------------------------------------------------------------------------------------------------------------------------------------------------------------

            //creates a delay to prevent insta kills
            if (delaytimer >= 0)
            {
                delaytimer -= Time.deltaTime;
            }
            if (delaytimer < 0.1f)//sets it to negative on final iteration
            {
                delaytimer = -1;
            }
            if ( housing < people && delaytimer < 0)// if delay timer is done and you are in trouble - time for killing
            {
                badtime += Time.deltaTime * (1 + (people - housing) / people);
            }
            else if (housing >= people && badtime > 0 && !gameover) // if you are good - start reducing badtime
            {
                badtime -= Time.deltaTime * (1 + (housing - people) / people);
            }
			else if ( housing >= people && badtime < 0 && !gameover)//badtime is gone - back to good!
            {
                bad = false;
                badtime = 0;
            }

            //main people evaulation

			if ((housing >= people && !bankrupt) || delaytimer > 0)
            {
	
                if (delaytimer < 0) // delay timer system only resets when you go back to normal mode
                {
                    delaytimerdone = true;
                }

                if (people < 1200) //different population growths under 1200 and over
                {
					deltaPopulation = Mathf.Pow (people, 0.75f) * POPGROWTH;
                }
				else
                {
                    if (inittime == 0) //on the first frame of the switch to post 1200 curve 
                    {
                        inittime = time;
                        initvalue = Mathf.Pow(people, 0.75f) * POPGROWTH; //get a baseline value
                    }
					deltaPopulation = (initvalue / (1 + (Mathf.Pow (2.71828f, 0.05f * (time - inittime - 100))))); //slowly divide that value by a bigger and bigger number
				}
            }

			else if (housing < people &&  !bankrupt ) // and badtime can be anything this only triggers when badtime=0 but badtime increases
            { //if your people is less than your housing
				if (delaytimerdone && people - housing > 30)
                {
                    delaytimerdone = false;
                    delaytimer = 5; // warning time

                }
                bad = true;
				deltaPopulation = Mathf.Pow (people, 0.75f) * (POPGROWTH / (badtime + 1));

            }

            else
            {
				
                gameover = true;
				deltaPopulation = Mathf.Pow(people, 0.75f) * (-0.01f * badtime);
                if (people < 0)
                {
                    people = 0;
                    money = 0;
                    youLose = true;
                }
            }

			people += deltaPopulation;
			popRate.text = ((int)(deltaPopulation * 60 / Time.deltaTime)).ToString() + "/minute";

            //Money Stuff----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            if ((housing >= people && !bankrupt) || delaytimer > 0)
            {
				deltaMoney = people * 2 * Time.deltaTime * (1 + moneyboost / 100);

            }
			else if (!bankrupt && housing < people)
            {
				deltaMoney = (people * (2 - badtime) * Time.deltaTime);
            }



			money += deltaMoney;
			moneyRate.text = "$" +  ((int)(deltaMoney * 60 / Time.deltaTime)).ToString() + "/minute";

			if (money < 0)
			{
				bankrupt = true;
				money = 0;
			}	
            


            //Emissions stuff----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            Emissionsgain = 0;
            for (int i = 0; i < pplvl.Length; i++)
            {
                if (pplvl[i] != 0)
                {
                    Emissionsgain += (int)(Mathf.Pow(2, (float)pplvl[i] - 1) * coalPower) * (float)(1.1 - Mathf.Min(1.4f, Emissionsboost / 100)) * 1.8f;
                }

            }
            Emissionsgain -= Mathf.Pow(50.0f, (float)renewablescount / 3.0f) - 1;

            if (Emissionsgain < 0)
            {

                Emissionsgain = coalplants * 10 - 1;
            }
            Emissions += (int)Emissionsgain;
            EmFrac = Emissions / MAXEM;
            emissionBar.fillAmount = EmFrac;
			EmissionsText.text = ((int)(EmFrac * 100)).ToString() + " %";
       
			//Debug.Log (Emissionsgain);
            if (Emissionsgain <= 0)
            {
                EmissionsRateText.text = "Never";
            }
            else
            {				
					EmissionsRateText.text = ((int)(((MAXEM - Emissions) / Emissionsgain) * Time.deltaTime)).ToString() + " seconds";
            }



			if(lockPopulationForFirstFewSeconds)
			{
				people = StaticReset.StartPeople;
			}

            //pause
        }




		//storm stuff----------------------------------------------------------------------------------------------------------------------------------------------------
		if (Random.Range (0, MAXEM / Emissions) < 0.003)
		{
			int randTornadoPos = (int)Random.Range (0, 7);

			if (coalobjects [randTornadoPos] != null && tornadoActive[randTornadoPos] != true)
			{
				tornadoActive [randTornadoPos] = true;
				//intent  1 minus a nubmer  in a range from (25 times fraction of maxEmissions) to 30 , minus perk bonus  - convert to factions
				money *= (1-  (Random.Range(25 * EmFrac , 50 * EmFrac) * ((100-Emissionsboost)/100)) / 100);
				deadplant [randTornadoPos] = true;
				Instantiate (tornado, new Vector3 (earth.transform.position.x, earth.transform.position.y, -6), Quaternion.Euler (new Vector3 (0, 0, -45 * randTornadoPos)));
			}
		}


		//Power Plant dragging stuff--------------------------------------------------------------------------------------------------------------------------------------
        if (drag)
        {
            plantAvailability();
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            float dragrot = Mathf.Rad2Deg * Mathf.Atan((mousePos.y - earth.transform.position.y) / (mousePos.x - earth.transform.position.x));
            if (Input.GetMouseButton(0) && mousePos.x < 1)
            {
                coaldraggable.transform.position = new Vector3(10000, 10000, -5);
                RNdraggable.transform.position = new Vector3(10000, 10000, -5);

                if ((mousePos.x - earth.transform.position.x) > 0)
                {

                    tempdrag.transform.rotation = Quaternion.Euler(new Vector3(0, 0, lockAngle(dragrot - 90)));
                }
                else
                {
                    tempdrag.transform.rotation = Quaternion.Euler(new Vector3(0, 0, lockAngle(dragrot - 270)));
                }
                tempdrag.transform.position = new Vector3(earth.transform.position.x, earth.transform.position.y, -5);

            }
            else if (Input.GetMouseButton(0) && mousePos.x >= 1)
            {
                tempdrag.transform.position = new Vector3(1000, 10000, -5);
                if (coaldrag)
                {

					coaldraggable.transform.position = new Vector3(mousePos.x, mousePos.y, -5);
                }
                else
                {
					RNdraggable.transform.position = new Vector3(mousePos.x, mousePos.y, -5);
                }
            }
            else
            {
                coaldraggable.transform.position = new Vector3(10000, 10000, -5);
                RNdraggable.transform.position = new Vector3(10000, 10000, -5);
                if ((mousePos.x - earth.transform.position.x) > 0)
                {

                    tempdrag.transform.rotation = Quaternion.Euler(new Vector3(0, 0, lockAngle(dragrot - 90)));
                }
                else
                {
                    tempdrag.transform.rotation = Quaternion.Euler(new Vector3(0, 0, lockAngle(dragrot - 270)));
                }
                int coalNumber = plantNumber(tempdrag.transform.rotation.eulerAngles.z);

                if (pplvl[coalNumber] == 0 && RNobjects[coalNumber] == null && mousePos.x < 1)
                {
                    if (coaldrag)
                    { // coal drop
						               
                        coalobjects[coalNumber] = tempdrag;
                        coalobjects[coalNumber].name = "coal" + coalNumber.ToString();
                        pplvl[coalNumber]++;
                        coalplants++;
                        money -= coalPlantCost;
						coalPlantCost = Mathf.Min((int)(coalPlantCost * nextupcost) , 8000);
                        coaldrag = false;
                        GameObject poofloc = tempdrag.transform.GetChild(0).transform.GetChild(0).gameObject;
                        Instantiate(smokepoof, poofloc.transform.position, Quaternion.identity);
						if (tutorial.tutorialOn)
						{
							tutorial.okButtonBool = true;
							refresh ();
						}      
                    }
                    else if (pplvl[coalNumber] == 0)
                    { //renewable drop
                        int RNNumber = plantNumber(tempdrag.transform.rotation.eulerAngles.z);
                        RNobjects[RNNumber] = tempdrag;
                        RNobjects[RNNumber].name = "RN" + RNNumber.ToString();
                        renewablescount++;
                        money -= renewablecost;
                        RNdrag = false;

                    }
                    drag = false;
                }
                else
                {

                    GameObject.Destroy(tempdrag);

                }
                drag = false;
                coaldrag = false;
            }
        }
        else
		{
            plantAvailabilityReset();
        }





		//check to prevent glitch
		if (Input.GetMouseButton(0) ) 
		{
			Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			if (mousePos.x > 1 && mousePos.y > -2) 
			{
			upgradeMenu.GetComponent<Animator> ().SetBool ("upgradeMenuToggle", false);
			}
		}

    }
	//end of update-------------------------------------------------------------------------------------------------------------------------------------------------------------

    int firstPlantDeploy=0;
    bool greenOne;
    bool greenTwo;
    void plantAvailability()
    {
        if (firstPlantDeploy==1)
        {
            foreach (Transform child in plantAvailable.transform)
            {
                foreach (Transform grandchild in child.transform)
                {
                    if (grandchild.name == "panel")
                    {
                        //grandchild.GetComponent<Renderer>().enabled = true;
                    }
                }
            }
        }
        for (int i = 0; i < coalobjects.Length; i++)
        {
            foreach (Transform child in plantAvailable.transform)
            {
                foreach (Transform grandchild in child.transform)
                {

                    if (coalobjects[i] != null)
                    {
                        //Debug.Log(child.name[child.name.Length - 1].ToString() + ": " + coalobjects[i].name+": " + i.ToString());
                        if (child.name[child.name.Length - 1].ToString() == i.ToString())
                        {
                            if (grandchild.name == "crossMark")
                            {
                                grandchild.GetComponent<Renderer>().enabled = true;
                            }
                        }
                        else
                        {
                            if (grandchild.name == "panel")
                            {
                                greenOne = true;
                            }
                        }

                    }
                    else if (RNobjects[i] != null)
                    {

                        //Debug.Log(child.name[child.name.Length - 1].ToString() + ": " + coalobjects[i].name+": " + i.ToString());
                        if (child.name[child.name.Length - 1].ToString() == i.ToString())
                        {
                            if (grandchild.name == "crossMark")
                            {
                                grandchild.GetComponent<Renderer>().enabled = true;
                            }
                        }
                        else
                        {
                            if (grandchild.name == "panel")
                            {
                                greenTwo = true;
                            }
                        }
                    }
                    if (RNobjects[i] != null)
                    {
                        if (greenOne && greenTwo)
                        {
                            if (grandchild.name == "panel")
                            {
                                //grandchild.GetComponent<Renderer>().enabled = true;
                            }
                        }
                    }
                    else {
                        if (greenOne)
                        {
                            if (grandchild.name == "panel")
                            {
                                //grandchild.GetComponent<Renderer>().enabled = true;
                            }
                        }
                    }
                }
            }
        }
        firstPlantDeploy++;
    }

    void plantAvailabilityReset()
    {
        greenOne = false;
        greenTwo = false;
        foreach (Transform child in plantAvailable.transform)
        {
			foreach (Transform grandchild in child.transform)
			{
				grandchild.GetComponent<Renderer> ().enabled = false;       
			}
         }
    }

    public void CreatePowerPlant()
	{
        if (money >= coalPlantCost  && coalplants < 8) {
			tempdrag = (GameObject)Instantiate (smallcoal, Camera.main.ScreenToWorldPoint(Input.mousePosition), Quaternion.Euler (new Vector3 (0, 0, 0)));
			drag = true;
			coaldrag = true;
		}

	}
	public void upgradecoal()
	{
		if (teir2cost <= Backend.money && pplvl[UpgradeNumber]==1) {
			float tempuprot = Backend.coalobjects [UpgradeNumber].transform.rotation.eulerAngles.z;
			GameObject.Destroy (Backend.coalobjects [UpgradeNumber]);
			Backend.coalobjects[UpgradeNumber] = (GameObject)Instantiate (mediumcoal, new Vector3 (earth.transform.position.x, earth.transform.position.y, -5), Quaternion.Euler (new Vector3 (0, 0, tempuprot)));
			Backend.pplvl [UpgradeNumber]++;
			Backend.money -= teir2cost;
            tutorial.okButtonBool = true;
            tutorial.upgradeBool = true;
			refresh ();
        }
		else if (teir3cost <= Backend.money && pplvl[UpgradeNumber]==2) {
			float tempuprot = Backend.coalobjects [UpgradeNumber].transform.rotation.eulerAngles.z;
			GameObject.Destroy (Backend.coalobjects [UpgradeNumber]);
			Backend.coalobjects[UpgradeNumber] = (GameObject)Instantiate (bigcoal, new Vector3 (earth.transform.position.x, earth.transform.position.y, -5), Quaternion.Euler (new Vector3 (0, 0, tempuprot)));
			Backend.pplvl [UpgradeNumber]++;
			Backend.money -= teir3cost;
		}
	}

	public void destroycoal()
	{
		GameObject.Destroy (Backend.coalobjects [UpgradeNumber]);
		pplvl [UpgradeNumber] = 0;
		coalplants--;

		//coalPlantCost /= 2;
		GameObject.Find ("upgradeMenu").GetComponent<Animator> ().SetBool ("upgradeMenuToggle", false);
	}

	public void CreateRNPlant()
	{
		if (money >= renewablecost)
		{
			tempdrag = (GameObject)Instantiate (renewable, Input.mousePosition, Quaternion.Euler (new Vector3 (0, 0, 0)));
			drag = true;
			RNdrag = true;
		}
	}


	public static void refresh()
	{
		housing = minhousing;//minimum
		for (int i = 0; i < pplvl.Length; i++)
		{
			if (pplvl[i] != 0)
			{
				housing += (int)(Mathf.Pow(2, (float)pplvl[i] - 1) * (coalPower + 100 / 2.0) ); // housing is 2^lvl-1 * coal power
			}

		}
		housing += (int)(renewablescount * RNpower * (1 + RNenergyboost / 100));//renerables power
		housing = (int)(housing * (1 + energyboost / 100));
	}


	public int lockAngle(float degrees)
	{
		if (degrees > -22.5) {
			return 0;
		} 
		else if (degrees > -67.5)
		{
			return -45;
		} 
		else if (degrees > -112.5)
		{
			return -90;
		} 
		else if (degrees > -157.5) 
		{
			return -135;
		} 
		else if (degrees > -202.5) 
		{
			return -180;
		} 
		else if (degrees > -247.5)
		{
			return -225;
		}
		else if (degrees > -292.5) 
		{
			return -270;
		} 
		else if (degrees > -337.5)
		{
			return -315;
		}
		else
		{
			return 0;
		}
	}


	public int plantNumber(float angle)
	{
		if ((int)angle != 0)
		{
			int lvl = (360 - (int)angle) / 45;
			return lvl;
		} else 
		{
			return 0;
		}
	}
    float fadeScreenAlpha=0;
    Color fade = Color.white;
    IEnumerator fadeOut(string screen)
    {
        yield return new WaitForSeconds(0.03f);
        if (fadeScreenAlpha < 1)
        {
            fadeScreenAlpha += 0.02f;
            fade.a = fadeScreenAlpha;
            fadeScreen.color=fade;
            StartCoroutine(fadeOut(screen));
        }
        else
        {
            SceneManager.LoadScene(screen);
        }
        
    }

	IEnumerator loseSequence()
	{
		yield return new WaitForSeconds (10);

		StartCoroutine(fadeOut("lose"));
	}
	IEnumerator winSequence()
	{
		yield return new WaitForSeconds (10);
		StartCoroutine(fadeOut("win"));
	}
}
