using UnityEngine;
using System.Collections;

public class SkyChange : MonoBehaviour {

	Color orange = new Color(84f/255,203f/255,255f/255);
	Color purple = new Color(55f/255,11f/255,147f/255);
	Color black = new Color(0f/255,0f/255,0f/255);
	float t=0;

	bool changed = false;
	int colorChange=1;

	float switchTime=120;

	bool c1 = false;
	bool c2 = false;
	bool c3 = false;
	bool c4 = false;

	void Start()
	{
		//StartCoroutine (blackToPurple ());
	}
	// Update is called once per frame
	void Update () {
		if (colorChange == 1) {
			StartCoroutine (blackToPurple ());
			c1 = true;
			colorChange = 0;
		}
		else if (colorChange == 2) {
			StartCoroutine (purpleToOrange ());
			c2 = true;
			colorChange = 0;
		}
		else if (colorChange == 3) {
			StartCoroutine (orangeToPurple ());
			c3 = true;
			colorChange = 0;
		}
		else if (colorChange == 4) {
			StartCoroutine (purpleToBlack ());
			c4 = true;
			colorChange = 0;
		}

		if (c1) {
			changeColor (black, purple);
		}
		if (c2) {
			changeColor (purple, orange);
		}
		if (c3) {
			changeColor (orange, purple);
		}
		if (c4) {
			changeColor (purple, black);
		}
		//this.GetComponent<Camera>().backgroundColor = Color.Lerp(purple,orange,t);
	}
	IEnumerator blackToPurple()
	{
		yield return new WaitForSeconds(25);
		colorChange = 2;
		changed = false;
	}
	IEnumerator purpleToOrange()
	{
		yield return new WaitForSeconds(switchTime);
		colorChange = 3;
		changed = false;
	}
	IEnumerator orangeToPurple()
	{
		yield return new WaitForSeconds(20);
		colorChange = 4;
		changed = false;
	}
	IEnumerator purpleToBlack()
	{
		yield return new WaitForSeconds(switchTime);
		colorChange = 1;
		changed = false;
	}
	void changeColor(Color x, Color y)
	{
		
		if (!changed) {
			
			this.GetComponent<Camera> ().backgroundColor = Color.Lerp (x, y, t);
			//Debug.Log (t);
			if (t < 1) {
				t += 0.05f * Time.deltaTime;
			} 
			else {

				changed = true;
				t = 0;
				this.GetComponent<Camera> ().backgroundColor = Color.Lerp (x, y, 1);
				

				c1 = false;
				c2 = false;
				c3 = false;
				c4 = false;
				//Debug.Log (colorChange);
			}
		}

	}

}
