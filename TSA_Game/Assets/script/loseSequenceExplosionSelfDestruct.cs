﻿using UnityEngine;
using System.Collections;

public class loseSequenceExplosionSelfDestruct : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (waitAndDestroy ());
	}

	IEnumerator waitAndDestroy()
	{
		yield return new WaitForSeconds (4);
		Destroy (this.gameObject);
	}
}
