﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LMScript : MonoBehaviour {

    public Text loadingText;
    float loadCount;
    int bulletCount = 0;

    public Image loadingcircle;
	float timer = 0;
	bool done = false;


	// Use this for initialization
	void Start () {
        loadingText.text = "Loading";
		loadCount = 0;



    }
	
	// Update is called once per frame
	void Update () {
		
        loadCount++;
        //Debug.Log(loadCount * Time.deltaTime);
        if (loadCount * Time.deltaTime > 0.5f)
        {
            loadingText.text += ".";
            loadCount = 0;
            bulletCount++;
        }
        if (bulletCount % 4 == 0)
        {
            loadingText.text = "Loading";
        }


	
		if (timer >1  && !done) {
			
			SceneManager.LoadSceneAsync ("MainGame");
			done = true;
		}


		timer += Time.deltaTime;
		loadingcircle.fillAmount = timer;
		
	
	}
}
