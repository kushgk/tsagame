﻿using UnityEngine;
using System.Collections;

public class Moving : MonoBehaviour {

	// Use this for initialization
	float  i = 0;
	float initX;
	float initY;
	float initZ;
	bool switchDir;
	public float speed = 0.001f;
	public float distance = 0.5f;
	void Start () {
		i = 0;
		initX = transform.position.x;
		initY = transform.position.y;
		initZ = transform.position.z;
		switchDir = true;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.position = new Vector3(initX + i , initY , initZ );
		//i++;
		if (i <= -1* distance) {
			switchDir=true;
		} 
		if(i>=distance){
			switchDir=false;
		}
//		Debug.Log (i);
		if(switchDir){
			i+=speed;

		}
		else{
			i-=speed;
		}
	
	}
}
