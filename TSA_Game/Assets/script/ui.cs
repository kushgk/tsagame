﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; 

public class ui : MonoBehaviour {
	public GameObject menu1;
	public GameObject menu2;
	public GameObject menu2Button;
	public GameObject nonRenewable;
	public GameObject renewable;
	public GameObject buynrnButton;
	public GameObject buyrnButton;
	public GameObject windmill;


	public GameObject pause;
	public GameObject pauseClicked;
	Vector3 initbuttonpos ;

	float delay = -1;


	public static bool gamePause = false;
	bool menu2Toggle;
	bool menu2ToggleReady;
	float menu2ButtonInitSize;
	float menu2ButtonInitRotation;
	float menu2ButtonSizeTime=0;
	float timer = 0;
	int wholeTimer = 0;
	bool firstDeselect = false;
	public static bool readyToBuyPerk = false;
	// Use this for initialization
	void Start () {
		//menu1=GameObject.Find ("menu1");
		//menu2=GameObject.Find ("menu2");
		//menu2Button = GameObject.Find ("menu2Button");
		//nonRenewable = GameObject.Find ("nonRenewable");
		//renewable = GameObject.Find ("renewable");

		pause.active=true;
		pauseClicked.SetActive(false);


		menu2ButtonInitSize = menu2Button.transform.localScale.x;
		menu2.active = false;
		renewable.active = false;
		gamePause = false;
		menu2Toggle = false;
		menu2ToggleReady = true;


		initbuttonpos = menu2Button.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (wholeTimer != (int)timer)
		{

			wholeTimer = (int)timer;
			if (perks.buttonWiggle) {
				delay = 0.3f;
			}

		}
		if (!firstDeselect && tutorial.tutorial2On) {
			firstDeselect = true;
			if (menu2Toggle &&  menu2ToggleReady)
			{
				menu2Toggle = false;
				menu1.active = true;
				menu2.active = false;
				gamePause = false;
				menu2ToggleReady = false;
			}
			
		}


		timer += Time.deltaTime;
		delay -= Time.deltaTime;
		if (delay > 0 && menu1.active) {
			//Debug.Log (menu2Button.transform.position.x);
		//	menu2Button.GetComponent<Animator>().SetBool("menu2ButtonFlag" , true);
			menu2Button.transform.position = new Vector3 (initbuttonpos.x + 0.1f * Mathf.Sin (timer * 40), initbuttonpos.y);
		}
		else 
		{
		//menu2Button.GetComponent<Animator>().SetBool("menu2ButtonFlag" , false);
			menu2Button.transform.position = initbuttonpos;
		}




		float speed = 8;
		float size = 1.2f;
		float rotatationSpeed = speed*36;

		if (Input.GetMouseButton(0)  && !tutorial.tutorialOn) {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);

			if (mousePos.x < 1 ) {

				if (menu2Toggle &&  menu2ToggleReady)
				{
					menu2Toggle = false;
					menu1.active = true;
					menu2.active = false;
					gamePause = false;
					menu2ToggleReady = false;
				}

			}



		}
		if (!menu2ToggleReady) 
		{
			if (menu2Toggle) 
			{
				menu2Button.transform.localScale = new Vector2 (Mathf.Lerp (menu2ButtonInitSize, menu2ButtonInitSize * size, menu2ButtonSizeTime), Mathf.Lerp (menu2ButtonInitSize, menu2ButtonInitSize * size, menu2ButtonSizeTime));
				menu2Button.transform.Rotate (Vector3.back, rotatationSpeed * Time.deltaTime);

				if (menu2ButtonSizeTime < 1) 
				{
					menu2ButtonSizeTime += speed * Time.deltaTime;
				}
				else 
				{
					menu2Button.transform.localScale = new Vector2 (menu2ButtonInitSize * size, menu2ButtonInitSize * size);
					menu2Button.transform.localEulerAngles = new Vector3(menu2Button.transform.localRotation.x,menu2Button.transform.localRotation.y,-45);
					menu2ButtonSizeTime = 0;
					menu2ToggleReady = true;
				}
			} 
			else if (menu2Button.transform.localScale.x != menu2ButtonInitSize) 
			{
				menu2Button.transform.localScale = new Vector2 (Mathf.Lerp (menu2ButtonInitSize*size, menu2ButtonInitSize, menu2ButtonSizeTime), Mathf.Lerp (menu2ButtonInitSize*size, menu2ButtonInitSize, menu2ButtonSizeTime));
				menu2Button.transform.Rotate (Vector3.back, -rotatationSpeed * Time.deltaTime);
			
				if (menu2ButtonSizeTime < 1) 
				{
					menu2ButtonSizeTime += speed * Time.deltaTime;
				}
				else 
				{
					menu2Button.transform.localScale = new Vector2 (menu2ButtonInitSize, menu2ButtonInitSize);
					menu2Button.transform.localEulerAngles = new Vector3(menu2Button.transform.localRotation.x,menu2Button.transform.localRotation.y,0);
					menu2ButtonSizeTime = 0;
					menu2ToggleReady = true;
				}
			}
		}


	
	}
	public void menu1ToggleFunc(){
		
		menu1.active = true;
		menu2.active = false;
		gamePause = false;
	}
	public void menu2ToggleFunc(){
		
		if (GameObject.Find ("upgradeMenu") != null) {
			GameObject g = GameObject.Find ("upgradeMenu");
			g.GetComponent<Animator> ().SetBool ("upgradeMenuToggle", false);
			g.transform.position = new Vector3 (g.transform.position.x, -382.8f, g.transform.position.z); 
			if (tutorial.tutorialCount == 7 || tutorial.tutorialCount == 11) {
				tutorial.okButtonBool = true;
			}

		}
	
		if (!menu2Toggle && menu2ToggleReady)
		{
			menu2Toggle = true;
			menu1.active = false;
			menu2.active = true;
			//gamePause = true; //pauses game
			menu2ToggleReady = false;
		} 
		else if (menu2Toggle &&  menu2ToggleReady)
		{
			menu2Toggle = false;
			menu1.active = true;
			menu2.active = false;
			gamePause = false;
			menu2ToggleReady = false;
		}
	}

	public void pauseGame(){
		
			gamePause = true;

            //pause.active = false;
            pauseClicked.SetActive(true);
			Time.timeScale = 0F;
		
	}
	public void unPauseGame()
	{
		gamePause = false;
		//pause.active = true;
		pauseClicked.SetActive(false);
		Time.timeScale = 0.8F;
	}



	public void nonRenewableToggle(){
		nonRenewable.active = true;
		renewable.active = false;
	}
	public void renewableToggle(){
	nonRenewable.active = false;
		renewable.active = true;
		if (tutorial.tutorialCount == 12) {
			tutorial.okButtonBool = true;
		}

	}

	public void RNclicked(){
		buyrnButton.GetComponent<UnityEngine.UI.Image>().color = new Vector4(255,255,255,0.5f);
		windmill.GetComponent<UnityEngine.UI.Image>().color = new Vector4(255,255,255,0.5f);
	}
	public void NRNclicked(){
		buynrnButton.GetComponent<UnityEngine.UI.Image>().color = new Vector4(255,255,255,0.5f);
	}
	public void RNunclicked(){
		buyrnButton.GetComponent<UnityEngine.UI.Image>().color = new Vector4(255,255,255,1f);
		windmill.GetComponent<UnityEngine.UI.Image>().color = new Vector4(255,255,255,1f);
	}
	public void NRNunclicked(){
		buynrnButton.GetComponent<UnityEngine.UI.Image>().color = new Vector4(255,255,255,1f);
	}




}
