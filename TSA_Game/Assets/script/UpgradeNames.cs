﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; 
public class UpgradeNames : MonoBehaviour {




	// Use this for initialization
	void Start () {

		
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static string setString(string name , int num)
	{
		if (name.Equals ("battery")) {
			perks.des.text = "<b>Centralized Power Production</b>: <i>Allow cities to institute centralized power production, increasing the number of people cities can support.</i>\n<b>(+3 Energy Production)</b>";
			switch (num) {
				case 1:
					return "Centralized Power Production 1";
				case 2:
					return "Centralized Power Production 2";
				case 3:
					return "Centralized Power Production 3";
				case 4:
					return "Centralized Power Production 4";
			}


		}
		if (name.Equals ("lobby")) {
			perks.des.text = "<b>Lobbying</b>: <i>Hire Lobbyists to prevent expensive anti-coal legislation from being passed.</i>\n<b>(+2 Revenue) (-1 Air Quality)</b>";
			switch (num) {
			case 1:
				return "Lobbying 1";
			case 2:	   
				return "Lobbying 2";
			case 3:	   
				return "Lobbying 3";
			case 4:	
				return "Lobbying 4";
			}

		}
		if (name.Equals ("maintainance")) {
			perks.des.text = "<b>Adaptation</b>: <i>Build storm defenses to increase toughness of cities and reduce inevitable damage from climate change.</i>\n<b>(+2 Storm Defence)</b>";
			switch (num) {
			case 1:
				return "Adaptation 1";
			case 2:	   
				return "Adaptation 2";
			case 3:	   
				return "Adaptation 3";
			case 4:	
				return "Adaptation 4";
			}

		}
		if (name.Equals ("efficiency")) {
			perks.des.text = "<b>Superconductive Wires</b>: <i>Increase conductivity of wires to increase efficiency of energy transfer and allow for cheaper electricity.</i>\n<b>(+2 Energy Production)</b>";
			switch (num) {
			case 1:
				return "Superconductive Wires 1";
			case 2:	 
				return "Superconductive Wires 2";
			case 3:	
				return "Superconductive Wires 3";
			case 4:	
				return "Superconductive Wires 4";
			}

		}
		if (name.Equals ("CCS")) {
			perks.des.text = "<b>Carbon Sequestration</b>: <i>Store carbon emissions underground to prevent them from causing warming.</i>\n<b>(+3 Air Quality) (-1 Revenue)</b>";
			switch (num) {
			case 1:
				return "Carbon Sequestration 1";
			case 2:	 
				return "Carbon Sequestration 2";
			case 3:	
				return "Carbon Sequestration 3";
			case 4:	
				return "Carbon Sequestration 4";
			}

		}
		if (name.Equals ("RNbattery")) {
			perks.des.text = "<b>Decentralized Power Storage</b>: <i>Use in-home power storage options to increase grid stability and improve renewable efficiency.</i>\n<b>(+2 Renewable Power)</b>";
			switch (num) {
			case 1:
				return "Decentralized Power Storage 1";
			case 2:		
				return "Decentralized Power Storage 2";
			case 3:	  
				return "Decentralized Power Storage 3";
			case 4:	   
				return "Decentralized Power Storage 4";
			}

		}

		if (name.Equals ("trees")) {
			perks.des.text = "<b>Ecosystem rehabilitation</b>: <i>Plant trees and reintroduce species to create productive carbon scrubbing Ecosystems.</i>\n<b>(+2 Air Quality)</b>";
			switch (num) {
			case 1:
				return "Ecosystem rehabilitation 1";
			case 2:	 
				return "Ecosystem rehabilitation 2";
			case 3:	
				return "Ecosystem rehabilitation 3";
			case 4:	
				return "Ecosystem rehabilitation 4";
			}

		}
		if (name.Equals ("RNreasearch")) {
			perks.des.text = "<b>Sustainable Energy Research</b>: <i>Research renewable energy options, allowing for cheaper renewable energy production.</i>\n<b>(+2 RN Cost Reduction)</b>";
			switch (num) {
			case 1:
				return "Sustainable Energy Research 1";
			case 2:	
				return "Sustainable Energy Research 2";
			case 3:
				return "Sustainable Energy Research 3";
			case 4:	 
				return "Sustainable Energy Research 4";
			}

		}

		if (name.Equals ("EOS")) {
			perks.des.text = "<b>Economies of Scale</b>: <i>Increase Renewable production infrastructure drastically reducing the cost of production.</i>\n<b>(+3 RN Cost Reduction)</b>";
			switch (num) {
			case 1:
				return "Economies of Scale 1";
			case 2:		
				return "Economies of Scale 2";
			case 3:	
				return "Economies of Scale 3";
			case 4:	 
				return "Economies of Scale 4";
			}

		}
		if (name.Equals ("NG")) {
			perks.des.text = "<b>Alternative Fossil Fuels</b>: <i>Utilize cleaner burning alternatives to coal, such as natural gas, for energy production.</i>\n<b>(+2 Air Quality)</b>";
			switch (num) {
			case 1:
				return "Alternative Fossil Fuels 1";
			case 2:	
				return "Alternative Fossil Fuels 2";
			case 3:
				return "Alternative Fossil Fuels 3";
			case 4:	 
				return "Alternative Fossil Fuels 4";
			}

		}

		return "";

		
	}
}