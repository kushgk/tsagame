﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class building : MonoBehaviour
{
    public GameObject house;
    public GameObject apartment;
    public GameObject skyscraper1;
    public GameObject skyscraper2;
    public GameObject skyscraper3;

    public GameObject person1;
    public GameObject person2;

    public GameObject algae;
    public GameObject corn;

    GameObject earth;

    float tempPeopleNum = 1;
    float tempCharNum = 1;
    public static int hCount = 0; //how many buildings there are at the instance
    int baseCount = 0;

    int pCount = 0; //how many people there are at the instance

    int perkCount = 0; //how many perk buildings there are at the instance

    static int buildingNumber = 25;
    static int perkNumber = 20;
    static int ArrayRange = buildingNumber - 1;

    float[] buildingPos = new float[buildingNumber];

    float[] peoplePos = new float[30];

    float[] perkPos = new float[perkNumber];

    public static bool perkTrigger = false;
    public static string perkType;
    float peopleNum = 100;


    GameObject magnify;
    GameObject generated;

    public static int destructionNum = 0;
    float destructionTimerCount = 0;


    // Use this for initialization
    void Start()
    {
        magnify = GameObject.Find("magnifyBuildings");
        earth = GameObject.Find("earth6");
        for (int i = 0; i < buildingNumber; i++)
        {
            buildingPos[i] = i;
        }
        for (int i = 0; i < buildingNumber; i++)
        {
            var tempBpos = buildingPos[i];
            int r = Random.Range(i, buildingNumber);
            buildingPos[i] = buildingPos[r];
            buildingPos[r] = tempBpos;
            buildingPos[i] = buildingPos[i] * (360 / buildingNumber);
        }

        for (int i = 0; i < perkNumber; i++)
        {
            perkPos[i] = i;
        }
        for (int i = 0; i < perkNumber; i++)
        {
            var tempPrpos = perkPos[i];
            int rpr = Random.Range(i, perkNumber);
            perkPos[i] = perkPos[rpr];
            perkPos[rpr] = tempPrpos;
            perkPos[i] = perkPos[i] * (360 / perkNumber);
        }

        for (int i = 0; i < 30; i++)
        {
            float tempPerlin = Random.Range(0, 100);
            peoplePos[i] = Random.Range(0, 359);

        }
    }

    // Update is called once per frame
    
    void Update()
    {
        if (Backend.housing < Backend.people)
        {
            if (destructionTimerCount < Random.Range(1, 3))
            {
                destructionTimerCount += Time.deltaTime;
            }
            else {
                DestructionTimer();
                destructionTimerCount = 0;
            }
            
        }

        //Debug.Log (SceneManager.GetActiveScene().name);
        if (SceneManager.GetActiveScene().name.Equals("MainGame"))
        {
            //Debug.Log ("HELLO");
            peopleNum = Backend.people;
        }
        else
        {
            peopleNum = peopleNum + Mathf.Pow(peopleNum, 0.75f) * .008f * 0.2f;
        }


        if (Mathf.Pow(peopleNum, 0.4135f) >= tempPeopleNum)
        {
            generateBuilding();
            generatePeople();
            tempPeopleNum++;
        }

        if (Mathf.Pow(peopleNum, 0.4135f) >= tempCharNum)
        {
            //Debug.Log (peopleNum);
            //generatePeople();
            tempCharNum++;
        }

        if (perkTrigger)
        {
            generatePerkBuildings(perkType);
        }
    }
    void generateBuilding()
    {

        if (baseCount < buildingNumber)
        {
            baseCount++;
        }

        if (hCount < ArrayRange)
        {
            //hCount++;
        }

        else
        {
            return;
        }

        generated = Instantiate(apartment, new Vector3(earth.transform.position.x, earth.transform.position.y, transform.position.z), Quaternion.Euler(new Vector3(0, 0, buildingPos[hCount]))) as GameObject;
        generated.transform.parent = magnify.transform;
        generated.transform.localScale = new Vector3(1, 1, 1);
        if (baseCount % 2 == 0)
        {
            if (hCount < ArrayRange)
            {
                hCount++;
            }
            else
            {
                return;
            }
            //buildingRotation = Random.Range(0,360);

            generated = Instantiate(house, new Vector3(earth.transform.position.x, earth.transform.position.y, transform.position.z), Quaternion.Euler(new Vector3(0, 0, buildingPos[hCount]))) as GameObject;
            generated.transform.parent = magnify.transform;
            generated.transform.localScale = new Vector3(1, 1, 1);
            //Debug.Log(hCount);
            //buildingArray[hCount - 1] = generated;
            //aCount++;
        }
        if (baseCount % 3 == 0)
        {
            if (hCount < ArrayRange) { hCount++; }
            else
            {
                return;
            }
            //buildingRotation = Random.Range(0,360);
            generated = Instantiate(skyscraper1, new Vector3(earth.transform.position.x, earth.transform.position.y, transform.position.z), Quaternion.Euler(new Vector3(0, 0, buildingPos[hCount]))) as GameObject;
            generated.transform.parent = magnify.transform;
            generated.transform.localScale = new Vector3(1, 1, 1);
            //buildingArray[hCount - 1] = generated;
            //ss1Count++;
        }
        if (baseCount % 4 == 0)
        {
            if (hCount < ArrayRange) { hCount++; }
            else
            {
                return;
            }
            //buildingRotation = Random.Range(0,360);
            generated = Instantiate(skyscraper2, new Vector3(earth.transform.position.x, earth.transform.position.y, transform.position.z), Quaternion.Euler(new Vector3(0, 0, buildingPos[hCount]))) as GameObject;
            generated.transform.parent = magnify.transform;
            generated.transform.localScale = new Vector3(1, 1, 1);
            //buildingArray[hCount - 1] = generated;
            //ss2Count++;
        }
        if (baseCount % 5 == 0)
        {
            if (hCount < ArrayRange) { hCount++; }
            else
            {
                return;
            }
            //buildingRotation = Random.Range(0,360);
            generated = Instantiate(skyscraper3, new Vector3(earth.transform.position.x, earth.transform.position.y, transform.position.z), Quaternion.Euler(new Vector3(0, 0, buildingPos[hCount]))) as GameObject;
            generated.transform.parent = magnify.transform;
            generated.transform.localScale = new Vector3(1, 1, 1);
            //buildingArray[hCount - 1]=generated;
        }


    }


    void generatePerkBuildings(string type)
    {
        /*
		if (type == "algae") {
			Instantiate (algae, new Vector3 (earth.transform.position.x, earth.transform.position.y, transform.position.z), Quaternion.Euler (new Vector3 (0, 0, perkPos [perkCount])));
			perkCount++;
		}
		if (type == "corn") {
			Instantiate (corn, new Vector3 (earth.transform.position.x, earth.transform.position.y, transform.position.z), Quaternion.Euler (new Vector3 (0, 0, perkPos [perkCount])));
			perkCount++;
		}
		perkTrigger = false;
		*/
    }


    void generatePeople()
    {
        //Debug.Log ("PEOPLE!!!");
        if (pCount < 30) { pCount++; }
        var tempPchoose = Random.Range(1, 5);
        if (tempPchoose < 7)
        {
            generated = Instantiate(person1, new Vector3(earth.transform.position.x, earth.transform.position.y, transform.position.z), Quaternion.Euler(new Vector3(0, 0, peoplePos[pCount]))) as GameObject;
            generated.transform.parent = magnify.transform;
            generated.transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            generated = Instantiate(person2, new Vector3(earth.transform.position.x, earth.transform.position.y, transform.position.z), Quaternion.Euler(new Vector3(0, 0, peoplePos[pCount]))) as GameObject;
            generated.transform.parent = magnify.transform;
            generated.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    void DestructionTimer()
    {
        destructionNum++;
        //Debug.Log("num"+destructionNum);
    }
}