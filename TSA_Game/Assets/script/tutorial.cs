﻿using UnityEngine;
using System.Collections;

public class tutorial : MonoBehaviour {
	const int TUTLENGTH = 15;
	const int firstTutLength = 10;
	public GameObject[] tutorialPanels = new GameObject[TUTLENGTH];

    public static int tutorialCount = 0;
    public static bool tutorialOn = true;
	public static bool tutorial2On = false;
	public bool tutorialMode = false;
    public GameObject coalplant;
    public static bool okButtonBool;
    public static bool upgradeBool = false;
	bool firstFrame = false;


	public GameObject tornado;
    public GameObject pauseClicked;
	int perkTriggers = 0;

	public GameObject arrowPanel4;

    public bool resetTutorial;

    // Use this for initialization
    void Start()
    {
        if (PlayerPrefs.GetInt("firstTime") == 0)
        {            
            tutorialOn = true;
			tutorialCount = 0;
			upgradeBool = false;

            PlayerPrefs.SetInt("firstTime", 1);

			tutorialMode = true;
			tutorial2On = false;


            
        }
        else if (PlayerPrefs.GetInt("firstTime") == 1)
        {
            tutorialOn = false;
			tutorialMode = false;
			tutorial2On = false;
        }
        if (tutorialOn)
        {
            tutorialPanels[0].SetActive(true);
            
        }

    }
	
	// Update is called once per frame
	void Update () {
		
        if(resetTutorial)
        {
            //Debug.Log(PlayerPrefs.GetInt("firstTime"));
            PlayerPrefs.SetInt("firstTime", 0);
            ui.gamePause = true;
            //Debug.Log(PlayerPrefs.GetInt("firstTime"));
            resetTutorial = false;
        }


		if (!firstFrame) {
			firstFrame = true;
			if (tutorialOn) {
			   ui.gamePause = true;
			}
		}

		if (tutorialCount == 0 && tutorialOn) {
			Backend.money = 9000;
		


		}

		if (tutorialCount == 1) {
			ui.gamePause = false;
			StartCoroutine(WaitAndPause());
			if (Backend.coaldrag) {
				coalplant.SetActive (false);
			} else {
				coalplant.SetActive (true);
			}
			if (okButtonBool) {
				okButton ();
				okButtonBool = false;
			}

		} else if (tutorialCount == 3) {
			//Debug.Log ("hi");
			GameObject cgo;
			if (GameObject.Find ("coal0") != null) {
				cgo = GameObject.Find ("coal0");
			} else if (GameObject.Find ("coal1") != null) {
				cgo = GameObject.Find ("coal1");
			} else if (GameObject.Find ("coal2") != null) {
				cgo = GameObject.Find ("coal2");
			} else if (GameObject.Find ("coal3") != null) {
				cgo = GameObject.Find ("coal3");
			} else if (GameObject.Find ("coal4") != null) {
				cgo = GameObject.Find ("coal4");
			} else if (GameObject.Find ("coal5") != null) {
				cgo = GameObject.Find ("coal5");
			} else if (GameObject.Find ("coal6") != null) {
				cgo = GameObject.Find ("coal6");
			} else if (GameObject.Find ("coal7") != null) {
				cgo = GameObject.Find ("coal7");
			} else if (GameObject.Find ("coal8") != null) {
				cgo = GameObject.Find ("coal8");
			} else {
				cgo = null;
			}

			arrowPanel4.transform.SetParent (cgo.transform.GetChild(0));
			arrowPanel4.transform.localPosition =new Vector2(-1, 0);
			arrowPanel4.transform.localEulerAngles = new Vector3 (0, 0, 0);
			//Debug.Log (arrowPanel4.transform.position);
			if (okButtonBool) {
				
				arrowPanel4.SetActive (false);
				okButton ();

				okButtonBool = false;
			}
		} else if (tutorialCount == 4) {
			
			if (okButtonBool && upgradeBool) {
				okButton ();

				okButtonBool = false;
			}
		} else if (tutorialCount == 5) {

			if (Backend.coalobjects [3] != null && Backend.tornadoActive [3] != true) {
				Backend.money *= 0.8f;
				Backend.tornadoActive [3] = true;
				Instantiate (tornado, new Vector3 (Backend.earth.transform.position.x, Backend.earth.transform.position.y, -6), Quaternion.Euler (new Vector3 (0, 0, -45 * 3)));
				Backend.upgradeMenu.GetComponent<Animator> ().SetBool ("upgradeMenuToggle", false);


			}
			if (okButtonBool && upgradeBool) {
				
						
				okButton ();

				okButtonBool = false;
			}
		} else if (tutorialCount == 7 || tutorialCount==11 || tutorialCount == 12) {
			if (okButtonBool && upgradeBool) {
				
				okButton ();

				okButtonBool = false;
			}
		} else if (tutorialCount == 8 || tutorialCount == 13) {
			
			if (okButtonBool && upgradeBool) {
				perkTriggers++;
				if (perkTriggers == 2) {
					okButton ();
					perkTriggers = 0;
				}
				okButtonBool = false;

			}
		} 

		else if (Backend.money > 16000 && tutorialMode && !tutorial2On) {
			activateTutorial2 ();				
			tutorial2On = true;
			Backend.upgradeMenu.GetComponent<Animator> ().SetBool ("upgradeMenuToggle", false);
		}


    }
    void okButton()
    {
		
		if (tutorialOn && !tutorial2On) {
            
			if (tutorialCount < firstTutLength - 1) {
				ui.gamePause = true;
				tutorialPanels [tutorialCount].SetActive (false);
				tutorialCount++;
				//Debug.Log(tutorialCount);
				tutorialPanels [tutorialCount].SetActive (true);
			} else {
				
				ui.gamePause = false;
				tutorialOn = false;
				tutorialPanels [tutorialCount].SetActive (false);
				tutorialCount++;
			}
		} 

		else if (tutorial2On) {
			if (tutorialCount < TUTLENGTH - 1) {
				ui.gamePause = true;
				tutorialPanels [tutorialCount].SetActive (false);
				tutorialCount++;
				//Debug.Log(tutorialCount);
				tutorialPanels [tutorialCount].SetActive (true);
			} else {
				ui.gamePause = false;
				tutorialOn = false;
				tutorialPanels [tutorialCount].SetActive (false);
			}
		}
    }

    public void activateTutorial() {
        tutorialOn = true;
        ui.gamePause = true;
        //resetTutorial = true;
		for (int i = 0; i < TUTLENGTH-1; i++)
        {
            tutorialPanels[i].SetActive(false);
        }
        tutorialPanels[0].SetActive(true);
        pauseClicked.SetActive(false);
        Time.timeScale = 0.8F;
    }

	public void activateTutorial2() {
		tutorialOn = true;
		ui.gamePause = true;
		//resetTutorial = true;
		for (int i = 0; i < TUTLENGTH-1; i++)
		{
			tutorialPanels[i].SetActive(false);
		}
		tutorialPanels[firstTutLength].SetActive(true);
		pauseClicked.SetActive(false);
		Time.timeScale = 0.8F;
	}
	IEnumerator WaitAndPause()
	{
		
		// suspend execution for 5 seconds
		yield return new WaitForSeconds(30);
		//ui.gamePause = true;
	}
}
