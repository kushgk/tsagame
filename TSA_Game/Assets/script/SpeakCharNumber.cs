﻿using UnityEngine;
using System.Collections;

public class SpeakCharNumber : MonoBehaviour {
    float speakFrequency = 50; //in seconds
    public static bool timer=false;
    void Start()
    {
        StartCoroutine(WaitMakeBox(speakFrequency));
    }
	public static int GetSpeakNumber () {
        //Debug.Log(Random.Range(1, CharacterControl.charNumber));
        return Random.Range(1, CharacterControl.charNumber);
	}
    IEnumerator WaitMakeBox(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        timer = true;
        StartCoroutine(WaitMakeBox(speakFrequency));
    }
}
