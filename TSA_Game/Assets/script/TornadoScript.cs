﻿using UnityEngine;
using System.Collections;
using System;

public class TornadoScript : MonoBehaviour {

	float time = 0;
	public GameObject bubblepop;
	GameObject earth;
	int angle;

	// Use this for initialization
	void Start () {
		earth = Backend.earth;

		angle = plantNumber ();

	}
	
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;

		if (time > 10) {
			
				Backend.tornadoActive [plantNumber ()] = false;


				//Instantiate (bubblepop, new Vector3 (earth.transform.position.x, earth.transform.position.y, -5), Quaternion.Euler (new Vector3 (0, 0, this.transform.rotation.eulerAngles.z)));
				GameObject.Destroy (this.gameObject);
		

		}

	}
	public int plantNumber()
	{
		if (this.transform.rotation.eulerAngles.z != 0) {
			int lvl = (360 - (int)this.transform.rotation.eulerAngles.z) / 45;
			return lvl;

		} else {
			return 0;
		}

	}
}
