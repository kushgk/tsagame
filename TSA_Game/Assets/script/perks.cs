﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; 
public class perks : MonoBehaviour {

	int costScale = 4;
	int renewableScale = 2;

	int initval = 500;

	int RNinitval = 4000;
	int dinitval = 0;
	int RNdinitval = 0;

	public GameObject costs;
	Text costsTxt;

	//non renewables
	public GameObject[] emissionsButtons;
	public GameObject[] costButtons;
	public GameObject[] energyButtons;
	public GameObject[] resistanceButtons;
	public GameObject[] BatteryButtons;
	public GameObject[] CSButtons;


	//renewables
	public GameObject[] RNtrees;
	public GameObject[] RNbattery;
	public GameObject[] RNefficiency;
	public GameObject[] RNEOS;
	public Text des1;
	public GameObject NRenewableparent;
	public GameObject Renewableparent;

	public GameObject lockObject;
	public static Text des;

	Text housing;
	public Text perkName;
	Button selected;

	//new variables
	int BatteryPerkLevel = 1;
	int TransmissionPerkLevel =1;
	int MaintainancePerkLevel = 1;
	int NatGasPerkLevel = 1;
	int LobbyistPerkLevel = 1;
	int CSPerkLevel = 1;


	int RNBatteryPerkLevel = 1;
	int RNtreesPerkLevel =1;
	int RNefficiencyPerkLevel = 1;
	int RNEOSPerkLevel = 1;

	public ArrayList allButtons;


	bool[] hasClicked = new bool[10];


	float currentcost=0;
	public static bool buttonWiggle;


	GameObject[] locks = new GameObject[21];
	GameObject[] RNlocks = new GameObject[15];



	float lockedButton = 0.5f;

	Vector4 stylizedBlue = new Vector4(90f/255f, 100f/255f, 180f/255f, 1);
	Vector4 stylizedRed = new Vector4(255f/255f, 86f/255f, 86f/255f, 1);
	Vector4 stylizedGreen = new Vector4(131f/255f, 234f/255f, 98f/255f, 1);

	// Use this for initialization
	void Start ()
	{



		dinitval = initval * 2;
		RNdinitval = RNinitval * 2;
		des = des1;

		costsTxt = costs.GetComponent<Text> ();


		ColorBlock ButtonColor = emissionsButtons [0].GetComponent<Button> ().colors;
		ButtonColor.normalColor = new Vector4 (lockedButton, lockedButton, lockedButton, 1);
		ButtonColor.highlightedColor = new Vector4 (lockedButton, lockedButton, lockedButton, 1);
		ButtonColor.pressedColor = new Vector4 (lockedButton - 0.1f, lockedButton - 0.1f, lockedButton - 0.1f, 1);


		allButtons = new ArrayList ();



		foreach (GameObject emissionsButton in emissionsButtons) {
			allButtons.Add (emissionsButton);

		}

		foreach (GameObject costButton in costButtons) {
			allButtons.Add (costButton);

			foreach (GameObject energyButton in energyButtons) {
				allButtons.Add (energyButton);

			}

			foreach (GameObject resistanceButton in resistanceButtons) {
				allButtons.Add (resistanceButton);
			}
			foreach (GameObject resistanceButton in BatteryButtons) {
				allButtons.Add (resistanceButton);
			}
			foreach (GameObject resistanceButton in CSButtons) {
				allButtons.Add (resistanceButton);
			}

			//RENEWABLES----------------------------------------------
			foreach (GameObject energyButton in RNbattery) {
				allButtons.Add (energyButton);
			}

			foreach (GameObject resistanceButton in RNefficiency) {
				allButtons.Add (resistanceButton);
			}
			foreach (GameObject resistanceButton in RNEOS) {
				allButtons.Add (resistanceButton);
			}
			foreach (GameObject resistanceButton in RNtrees) {
				allButtons.Add (resistanceButton);

			}


			foreach (GameObject button in allButtons) {
				button.GetComponent<Button> ().colors = ButtonColor;
			}


			lockperk ();

			perkName.text = "";
			des.text = "";
			costsTxt.text = "$$$";
		}
	}

	public void resetClick()
	{
		for (int i = 0; i < 10; i++)
		{
			hasClicked [i] = false;

		}
		foreach (GameObject button in allButtons) {
			button.transform.localScale  = new Vector3(0.01f, 0.01f, 0.01f);
		}
	}



	// Update is called once per frame
	void Update () {
		if (Backend.money > checkbuy ()) {
			buttonWiggle = true;
		}
		else
		{
			buttonWiggle = false;
		}

		if (currentcost != 0) {
			textcolor (currentcost);
		}
	}

	public float checkbuy()
	{
		int[] vars = new int[4];
		vars[0] =  TransmissionPerkLevel;
		vars[1] =  MaintainancePerkLevel;
		vars[2] =  NatGasPerkLevel ;
		vars[3] =  LobbyistPerkLevel;


		int min = 100;
		for (int i = 0; i < 4; i++)
		{
			if (min > vars [i])
			{
				min = vars [i];
			}

		}
		return  initval * Mathf.Pow (costScale, min); 
	}

	public void RNBatteryPerk(Button button){
		int PerkNumber =int.Parse(button.name.Substring (button.name.Length - 1));

		if (!hasClicked [0]  && PerkNumber == RNBatteryPerkLevel) {
			resetClick ();
			hasClicked [0] = true;
			RNBatterycost1 (button);


		}
		else
		{



			float cost =RNdinitval * Mathf.Pow (renewableScale, RNBatteryPerkLevel);
			currentcost = cost;

			currentcost = cost;

			if (Backend.money >= cost && PerkNumber == RNBatteryPerkLevel)
			{

				Backend.RNenergyboost *= 2; //old - KEEP
				Backend.RNenergyboost += 100;
				Backend.money -= cost;
				RNBatteryPerkLevel++;
				onPurchase(button);;
			}
		}


	}
	public void RNtreePerk(Button button){
		int PerkNumber =int.Parse(button.name.Substring (button.name.Length - 1));

		if (!hasClicked [1] && PerkNumber == RNtreesPerkLevel) {
			resetClick ();
			hasClicked [1] = true;
			RNTreecost1 (button);
		} 
		else 
		{


			float cost = RNinitval * Mathf.Pow (renewableScale, RNtreesPerkLevel);
			currentcost = cost;

			if (Backend.money >= cost && PerkNumber == RNtreesPerkLevel)
			{
				Backend.Emissionsboost += 10 - RNtreesPerkLevel; //original
				Backend.money -= cost;
				RNtreesPerkLevel++;
				onPurchase(button);
			}
		}



	}
	public void RNefficiencyPerk(Button button){ // switch costs with battery
		if (tutorial.tutorialCount == 13) {

			tutorial.okButtonBool = true;
		}
		int PerkNumber = int.Parse (button.name.Substring (button.name.Length - 1));

		if (!hasClicked [2] && PerkNumber == RNefficiencyPerkLevel) {
			resetClick ();
			hasClicked [2] = true;
			RNefficiencycost1 (button);
		} else {


			float cost = RNinitval * Mathf.Pow (renewableScale, RNefficiencyPerkLevel);
			currentcost = cost;

			if (Backend.money >= cost && PerkNumber == RNefficiencyPerkLevel) {

				Backend.renewablecost *= (0.5f - ((float)Backend.difficulty / 500)); //original 0.5f
				Backend.money -= cost;
				RNefficiencyPerkLevel++;
				onPurchase (button);
			}
		}


	}
	public void RNEOSPerk(Button button){
		int PerkNumber = int.Parse (button.name.Substring (button.name.Length - 1));
		if (!hasClicked [3]  && PerkNumber == RNEOSPerkLevel && RNefficiencyPerkLevel > 2 && RNBatteryPerkLevel > 2) {
			resetClick ();
			hasClicked [3] = true;
			RNEOScost1 (button);
		} 
		else {


			float cost = RNdinitval * Mathf.Pow (renewableScale, RNEOSPerkLevel + 2);
			currentcost = cost;



			if (Backend.money >= cost && PerkNumber == RNEOSPerkLevel && RNefficiencyPerkLevel > 2 && RNBatteryPerkLevel > 2) {
				Backend.renewablecost *= (0.6f - ((float)Backend.difficulty / 500)); // original 0.6f
				Backend.RNenergyboost += 100;
				Backend.money -= cost;
				RNEOSPerkLevel++;
				onPurchase(button);
			}
		}
	}



	//Renewable on hover------

	public void RNBatterycost1(Button button)
	{
		hoverAction(button);
		int PerkNumber =int.Parse(button.name.Substring (button.name.Length - 1));

		perkName.text = UpgradeNames.setString ("RNbattery", PerkNumber);

		float cost = RNdinitval * Mathf.Pow (renewableScale, PerkNumber);
		currentcost = cost;

		if (PerkNumber  == RNBatteryPerkLevel) {
			costsTxt.text = "$"+(cost).ToString ();
			textcolor (cost);
		}
		else if (PerkNumber < RNBatteryPerkLevel) {

			currentcost = 0;
			costsTxt.color = stylizedBlue;
			costsTxt.text = "Purchased";
			currentcost = 0;
		}
		else {
			currentcost = 0;
			costsTxt.color = stylizedRed;
			costsTxt.text = "Locked";
			currentcost = 0;
		}
	}
	public void RNTreecost1(Button button)
	{
		hoverAction(button);
		int PerkNumber =int.Parse(button.name.Substring (button.name.Length - 1));
		perkName.text = UpgradeNames.setString ("trees", PerkNumber);


		float cost = RNinitval * Mathf.Pow (renewableScale, PerkNumber);
		currentcost = cost;

		if (PerkNumber  ==RNtreesPerkLevel) {
			costsTxt.text = "$"+(cost).ToString ();
			textcolor (cost);
		} else if (PerkNumber  < RNtreesPerkLevel) {


			costsTxt.color = stylizedBlue;
			costsTxt.text = "Purchased";
			currentcost = 0;
		}

		else {
			costsTxt.color = stylizedRed;
			costsTxt.text = "Locked";
			currentcost = 0;
		}
	}
	public void RNEOScost1(Button button)
	{

		hoverAction(button);
		int PerkNumber =int.Parse(button.name.Substring (button.name.Length - 1));
		perkName.text = UpgradeNames.setString ("EOS", PerkNumber);
		float cost = RNdinitval * Mathf.Pow (renewableScale, PerkNumber + 2);
		currentcost = cost;

		if (PerkNumber == RNEOSPerkLevel && RNefficiencyPerkLevel > 2 && RNBatteryPerkLevel > 2) {
			costsTxt.text= "$"+(cost).ToString ();
			textcolor (cost);
		}else if (PerkNumber  < RNEOSPerkLevel) {
			costsTxt.color = stylizedBlue;
			costsTxt.text = "Purchased";
			currentcost = 0;
		}
		else {
			costsTxt.color = stylizedRed;
			costsTxt.text = "Locked";
			currentcost = 0;
		}

	}
	public void RNefficiencycost1(Button button)
	{
		hoverAction(button);
		int PerkNumber =int.Parse(button.name.Substring (button.name.Length - 1));
		perkName.text = UpgradeNames.setString ("RNreasearch", PerkNumber);

		float cost = RNinitval * Mathf.Pow (renewableScale, PerkNumber);
		currentcost = cost;

		if (PerkNumber == RNefficiencyPerkLevel) {
			costsTxt.text = "$"+(cost).ToString ();
			textcolor (cost);
		}else if (PerkNumber  < RNefficiencyPerkLevel) {
			costsTxt.color = stylizedBlue;
			costsTxt.text = "Purchased";
			currentcost = 0;
		}
		else {
			costsTxt.color = stylizedRed;
			costsTxt.text = "Locked";
			currentcost = 0;
		}
	}







	//NON RENEWABLE PERKS and methods--------------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void BatteryPerk(Button button){

		int PerkNumber = int.Parse (button.name.Substring (button.name.Length - 1));
		if (!hasClicked [4] && PerkNumber == BatteryPerkLevel && MaintainancePerkLevel > 2 && TransmissionPerkLevel > 2) {
			resetClick ();
			hasClicked [4] = true;
			Batterycost1 (button);
		}
		else {


			float cost = dinitval * Mathf.Pow (costScale, BatteryPerkLevel + 2);
			currentcost = cost;

			if (Backend.money >= cost && PerkNumber == BatteryPerkLevel && MaintainancePerkLevel > 2 && TransmissionPerkLevel > 2) {
				Backend.energyboost += 55;
				Backend.money -= cost;
				BatteryPerkLevel++;

				onPurchase(button);
			}
		}


	}

	public void TransmissionPerk(Button button){
		int PerkNumber = int.Parse (button.name.Substring (button.name.Length - 1));
		if (!hasClicked [5]  && PerkNumber == TransmissionPerkLevel) {
			resetClick ();
			hasClicked [5] = true;
			Transmissioncost1 (button);
		} else {


			float cost = initval * Mathf.Pow (costScale, TransmissionPerkLevel);
			currentcost = cost;

			if (Backend.money >= cost && PerkNumber == TransmissionPerkLevel) {
				resetClick ();
				Backend.energyboost += 35 + (int)(Backend.difficulty * 0.4f);
				Backend.moneyboost += 5;
				Backend.money -= cost;
				TransmissionPerkLevel++;
				onPurchase(button);

			}
		}
	}

	public void MaintainancePerk(Button button){	
		int PerkNumber = int.Parse (button.name.Substring (button.name.Length - 1));
		if (!hasClicked [6]  && PerkNumber == MaintainancePerkLevel) {
			resetClick ();
			hasClicked [6] = true;
			Maintainancecost1 (button);
		} else {


			float cost = initval * Mathf.Pow (costScale, MaintainancePerkLevel);
			currentcost = cost;

			if (Backend.money >= cost && PerkNumber == MaintainancePerkLevel) {
				resetClick ();
				Backend.Emissionsboost += 5;
				Backend.toughnessboost += 15;
				Backend.money -= cost;
				MaintainancePerkLevel++;
				onPurchase(button);


			}
		}
	}



	public void NatGasPerk(Button button){
		int PerkNumber = int.Parse (button.name.Substring (button.name.Length - 1));
		if (!hasClicked [7]  && PerkNumber == NatGasPerkLevel) {
			resetClick ();
			hasClicked [7] = true;
			NatGascost1 (button);
		} else {


			float cost = initval * Mathf.Pow (costScale, NatGasPerkLevel);
			currentcost = cost;

			if (Backend.money >= cost) {
				resetClick ();
				Backend.Emissionsboost += 14;
				Backend.moneyboost -= 3;
				Backend.money -= cost;
				NatGasPerkLevel++;
				onPurchase(button);

			}
		}
	}

	public void LobbyistPerk(Button button){


		if (tutorial.tutorialCount == 8) {
			
			tutorial.okButtonBool = true;
		}
		int PerkNumber = int.Parse (button.name.Substring (button.name.Length - 1));
		if (!hasClicked [8] && PerkNumber == LobbyistPerkLevel) {
			resetClick ();
			hasClicked [8] = true;
			Lobbyistcost1 (button);
		} else {


			float cost = initval * Mathf.Pow (costScale, LobbyistPerkLevel);
			currentcost = cost;
			if (Backend.money >= cost && PerkNumber == LobbyistPerkLevel ) {
				resetClick ();
				Backend.moneyboost += 15;
				Backend.Emissionsboost -= 5;
				Backend.money -= cost;
				LobbyistPerkLevel++;
				onPurchase(button);

			}
		}
	}
	public void CSPerk(Button button){
		int PerkNumber = int.Parse (button.name.Substring (button.name.Length - 1));
		if (!hasClicked [9] && PerkNumber == CSPerkLevel && NatGasPerkLevel > 1 && TransmissionPerkLevel > 1) {
			resetClick ();
			hasClicked [9] = true;
			CScost1 (button);
		} else {


			float cost = dinitval * Mathf.Pow (costScale, CSPerkLevel + 1);
			currentcost = cost;


			if (Backend.money >= cost ) {

				Backend.moneyboost -= 4;
				Backend.Emissionsboost += 20;
				Backend.money -= cost;
				CSPerkLevel++;
				onPurchase(button);
			}
		}
	}


	public void onPurchase(Button button)
	{
		refresh ();
		changebuttoncolor (button);
		resetClick ();
		costsTxt.color = stylizedBlue;
		costsTxt.text = "Purchased";
	}


	public void changebuttoncolor(Button button)
	{

		ColorBlock energyButtonColor = button.GetComponent<Button> ().colors;
		energyButtonColor.normalColor = Color.white;
		energyButtonColor.highlightedColor = Color.white;
		energyButtonColor.pressedColor = new Vector4 (0.9f,0.9f,0.9f,1);
		button.GetComponent<Button> ().colors = energyButtonColor;
	}


	public void Batterycost1(Button button)
	{
		hoverAction(button);
		int PerkNumber =int.Parse(button.name.Substring (button.name.Length - 1));

		perkName.text = UpgradeNames.setString ("battery", PerkNumber);


		float cost = dinitval * Mathf.Pow (costScale, PerkNumber + 2);
		currentcost = cost;

		if (PerkNumber  == BatteryPerkLevel  && MaintainancePerkLevel > 2 && TransmissionPerkLevel > 2) {


			costsTxt.text = "$"+(cost).ToString ();
			textcolor (cost);
		}
		else if (PerkNumber < BatteryPerkLevel) {
			costsTxt.color = stylizedBlue;
			costsTxt.text = "Purchased";
			currentcost = 0;
		}
		else {
			costsTxt.color = stylizedRed;
			costsTxt.text = "Locked";
			currentcost = 0;
		}

	}
	public void Transmissioncost1(Button button)
	{
		hoverAction(button);

		int PerkNumber =int.Parse(button.name.Substring (button.name.Length - 1));
		perkName.text = UpgradeNames.setString ("efficiency", PerkNumber);


		float cost = initval * Mathf.Pow (costScale, PerkNumber);
		currentcost = cost;
		if (PerkNumber  == TransmissionPerkLevel) {
			costsTxt.text = "$"+(cost).ToString ();
			textcolor (cost);
		} else if (PerkNumber  < TransmissionPerkLevel) {
			costsTxt.color = stylizedBlue;
			costsTxt.text = "Purchased";
			currentcost = 0;
		}

		else {
			costsTxt.color = stylizedRed;
			costsTxt.text = "Locked";
			currentcost = 0;
		}
	}
	public void Lobbyistcost1(Button button)
	{
		hoverAction(button);
		int PerkNumber =int.Parse(button.name.Substring (button.name.Length - 1));
		perkName.text = UpgradeNames.setString ("lobby", PerkNumber);


		float cost = initval * Mathf.Pow (costScale, PerkNumber);
		currentcost = cost;

		if (PerkNumber == LobbyistPerkLevel) {
			costsTxt.text= "$"+(cost).ToString ();
			textcolor (cost);
		}else if (PerkNumber  < LobbyistPerkLevel) {
			costsTxt.color = stylizedBlue;
			costsTxt.text = "Purchased";
			currentcost = 0;
		}
		else {
			costsTxt.color = stylizedRed;
			costsTxt.text = "Locked";
			currentcost = 0;
		}

	}
	public void NatGascost1(Button button)
	{
		hoverAction(button);
		int PerkNumber =int.Parse(button.name.Substring (button.name.Length - 1));
		perkName.text = UpgradeNames.setString ("NG", PerkNumber);

		float cost = initval * Mathf.Pow (costScale, PerkNumber);
		currentcost = cost;
		if (PerkNumber == NatGasPerkLevel) {
			costsTxt.text = "$"+(cost).ToString ();
			textcolor (cost);
		}else if (PerkNumber  < NatGasPerkLevel) {
			costsTxt.color = stylizedBlue;
			costsTxt.text = "Purchased";
			currentcost = 0;
		}
		else {
			costsTxt.color = stylizedRed;
			costsTxt.text = "Locked";
			currentcost = 0;
		}
	}
	public void CScost1(Button button)
	{
		hoverAction(button);
		int PerkNumber =int.Parse(button.name.Substring (button.name.Length - 1));
		perkName.text = UpgradeNames.setString ("CCS", PerkNumber);

		float cost = dinitval * Mathf.Pow (costScale, PerkNumber + 1);
		currentcost = cost;

		if (PerkNumber == CSPerkLevel && NatGasPerkLevel > 1 && TransmissionPerkLevel > 1) {
			costsTxt.text = "$"+(cost).ToString ();
			textcolor (cost);
		}else if (PerkNumber  < CSPerkLevel ) {
			costsTxt.color = stylizedBlue;
			costsTxt.text = "Purchased";
			currentcost = 0;
		}
		else {
			costsTxt.color = stylizedRed;
			costsTxt.text = "Locked";
			currentcost = 0;
		}
	}
	public void Maintainancecost1(Button button)
	{
		hoverAction(button);
		int PerkNumber =int.Parse(button.name.Substring (button.name.Length - 1));
		perkName.text = UpgradeNames.setString ("maintainance", PerkNumber);

		float cost = (initval * Mathf.Pow (costScale, PerkNumber));
		currentcost = cost;

		if (PerkNumber == MaintainancePerkLevel) {
			costsTxt.text = "$"+(cost).ToString ();
			textcolor (cost);
		}else if (PerkNumber  < MaintainancePerkLevel) {
			costsTxt.color = stylizedBlue;
			costsTxt.text = "Purchased";
			currentcost = 0;
		}
		else {
			costsTxt.color = stylizedRed;
			costsTxt.text = "Locked";
			currentcost = 0;
		}
	}


	public void hoverAction(Button button)//does things on every hover.
	{
		selected = button;
		button.transform.localScale = new Vector3 (0.012f, 0.012f, 1.0f);
	}



	public void refresh()
	{
		if (!tutorial.tutorialOn) {
			Backend.refresh ();
		}

		lockperk ();
	}








	public void blank()
	{
		selected.transform.localScale = new Vector3 (0.01f, 0.01f, 1.0f);
		costsTxt.text = "";
		des.text = "";
		des1.text = "";
		perkName.text = "";

	}



	public bool textcolor(float num)
	{
		if(!costsTxt.text.Equals("Purchased"))
		{
			if ( Backend.money >= num) {
				costsTxt.color = stylizedGreen;
				return true;
			} 
			else {
				costsTxt.color = stylizedRed;
				return false;
			}	
		}
		return false;

	}




	public void lockperk()
	{
		int i = 0;
		int j = 0;
		ColorBlock ButtonColor;






		foreach(GameObject Button in emissionsButtons){
			i++;j++;
			if (NatGasPerkLevel < i && locks[j] == null) {
				locks[j] = (GameObject)Instantiate (lockObject, Button.transform.position , Quaternion.identity);
				locks [j].transform.parent =NRenewableparent.gameObject.transform;
				//locks [j].transform.parent = this.gameObject;
			} 
			else if( NatGasPerkLevel >= i && locks[j] != null )
			{
				GameObject.Destroy (locks [j]);
			}
		}
		i = 0;
		foreach(GameObject Button in costButtons){

			i++;j++;
			if (LobbyistPerkLevel < i && locks[j] == null) {
				locks[j] = (GameObject)Instantiate (lockObject, Button.transform.position , Quaternion.identity);
				locks [j].transform.parent =NRenewableparent.gameObject.transform;
			} 
			else if( LobbyistPerkLevel >= i && locks[j] != null )
			{
				GameObject.Destroy (locks [j]);
			}
		}
		i = 0;
		foreach(GameObject Button in energyButtons){

			i++;j++;
			if (TransmissionPerkLevel< i && locks[j] == null) {
				locks[j] = (GameObject)Instantiate (lockObject, Button.transform.position , Quaternion.identity);
				locks [j].transform.parent =NRenewableparent.gameObject.transform;
			} 
			else if( TransmissionPerkLevel >= i && locks[j] != null )
			{
				GameObject.Destroy (locks [j]);
			}
		}

		i = 0;
		foreach(GameObject Button in resistanceButtons){

			i++;j++;
			if (MaintainancePerkLevel< i  && locks[j] == null) {
				locks[j] = (GameObject)Instantiate (lockObject, Button.transform.position , Quaternion.identity);
				locks [j].transform.parent =NRenewableparent.gameObject.transform;
			} 
			else if(MaintainancePerkLevel >= i && locks[j] != null )
			{
				GameObject.Destroy (locks [j]);
			}
		}
		i = 0;
		foreach(GameObject Button in BatteryButtons){

			i++;j++;
			if ((BatteryPerkLevel< i || MaintainancePerkLevel < 3 && TransmissionPerkLevel < 3) && locks[j] == null) {
				locks[j] = (GameObject)Instantiate (lockObject, Button.transform.position , Quaternion.identity);
				locks [j].transform.parent =NRenewableparent.gameObject.transform;
			} 
			else if(BatteryPerkLevel >= i && MaintainancePerkLevel > 2 && TransmissionPerkLevel > 2  && locks[j] != null )
			{
				GameObject.Destroy (locks [j]);
			}
		}
		i =0;
		foreach(GameObject Button in CSButtons){
			//Debug.Log (j);
			i++;j++;

			if ((CSPerkLevel < i ||  NatGasPerkLevel == 1 || TransmissionPerkLevel == 1) && locks[j] == null) {
				locks[j] = (GameObject)Instantiate (lockObject, Button.transform.position , Quaternion.identity);
				locks [j].transform.parent =NRenewableparent.gameObject.transform;
			} 
			else if(CSPerkLevel >= i &&  NatGasPerkLevel> 1 && TransmissionPerkLevel > 1 && locks[j] != null )
			{
				GameObject.Destroy (locks [j]);
			}
		}

		i = 0;

		j = 0;
		foreach(GameObject Button in RNbattery)
		{

			i++;j++;
			if (RNBatteryPerkLevel< i  && RNlocks[j] == null) {
				RNlocks[j] = (GameObject)Instantiate (lockObject, Button.transform.position , Quaternion.identity);
				RNlocks [j].transform.parent =Renewableparent.gameObject.transform;
			} 
			else if(RNBatteryPerkLevel >= i && RNlocks[j] != null )
			{
				GameObject.Destroy (RNlocks [j]);
			}
		}
		i = 0;
		foreach(GameObject Button in RNefficiency){

			i++;j++;
			if (RNefficiencyPerkLevel< i  && RNlocks[j] == null) {
				RNlocks[j] = (GameObject)Instantiate (lockObject, Button.transform.position , Quaternion.identity);
				RNlocks [j].transform.parent =Renewableparent.gameObject.transform;
			} 
			else if(RNefficiencyPerkLevel >= i && RNlocks[j] != null )
			{
				GameObject.Destroy (RNlocks [j]);
			}
		}

		i = 0;
		foreach(GameObject Button in RNEOS){

			i++;j++;
			if ((RNEOSPerkLevel< i || RNefficiencyPerkLevel <3 || RNBatteryPerkLevel <3) && RNlocks[j] == null) {
				RNlocks[j] = (GameObject)Instantiate (lockObject, Button.transform.position , Quaternion.identity);
				RNlocks [j].transform.parent = Renewableparent.gameObject.transform;
			} 
			else if(RNEOSPerkLevel >= i && RNefficiencyPerkLevel > 2 && RNBatteryPerkLevel > 2 && RNlocks[j] != null )
			{
				GameObject.Destroy (RNlocks [j]);
			}
		}
		i = 0;
		foreach(GameObject Button in RNtrees){

			i++;j++;
			if (RNtreesPerkLevel < i  && RNlocks[j] == null) {
				RNlocks[j] = (GameObject)Instantiate (lockObject, Button.transform.position , Quaternion.identity);
				RNlocks [j].transform.parent =Renewableparent.gameObject.transform;
			} 
			else if(RNtreesPerkLevel >= i && RNlocks[j] != null )
			{
				GameObject.Destroy (RNlocks [j]);
			}
		}





	}














}


